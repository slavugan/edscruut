from fake_useragent import UserAgent


class RandomNoMobileUserAgentMiddleware(object):
    def __init__(self):
        self.ua = UserAgent()

    def process_request(self, request, spider):
        request.headers.setdefault('User-Agent', self.get_useragent())

    def get_useragent(self):
        useragent = self.ua.random
        if 'iPad' in useragent or 'Android' in useragent:
            return self.get_useragent()
        else:
            return useragent

