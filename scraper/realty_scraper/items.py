# -*- coding: utf-8 -*-
import scrapy

class RealtyItem(scrapy.Item):
    property_type = scrapy.Field()
    property_type_type = scrapy.Field()
    deal = scrapy.Field()
    rooms = scrapy.Field()
    area = scrapy.Field()
    land_area = scrapy.Field()
    price = scrapy.Field()
    energy_class = scrapy.Field()
    termal_protection_class = scrapy.Field()
    country = scrapy.Field()
    city = scrapy.Field()
    address = scrapy.Field()
    latitude = scrapy.Field()
    longitude = scrapy.Field()
    year_built = scrapy.Field()
    url = scrapy.Field()
    new_building = scrapy.Field()
    floor = scrapy.Field()
    total_floors = scrapy.Field()
    description = scrapy.Field()
    bathroom = scrapy.Field()
    garden = scrapy.Field()
    garage = scrapy.Field()
    seller = scrapy.Field()
    terrace = scrapy.Field()
    balcony = scrapy.Field()
    phones = scrapy.Field()


    image_urls = scrapy.Field()
    images = scrapy.Field()

    id = scrapy.Field()
    model_obj = scrapy.Field()
    new_price = scrapy.Field()