from scraper.settings_common import *

BOT_NAME = 'scraper_realty'

SPIDER_MODULES = ['scraper.realty_scraper.spiders']
NEWSPIDER_MODULE = 'scraper.realty_scraper.spiders'

ITEM_PIPELINES = {
    'scraper.realty_scraper.pipelines.ObjModelPipeline': 300,
    'scraper.realty_scraper.pipelines.StoreImgPipeline': 301,
    'scraper.realty_scraper.pipelines.SaveImgModelPipeline': 302
}
