# -*- coding: utf-8 -*-
import hashlib
from django.utils import timezone
from scrapy.exceptions import DropItem
from scrapy.http import Request
from scrapy.pipelines.images import ImagesPipeline
from scrapy.utils.python import to_bytes

from vehicles.models import Country
from realty.models import PropertyType, PropertyTypeType, Seller, Realty, City, RealtyImage

PROPERTY_TYPES = PropertyType.objects.all().values_list('id', 'name_fr', 'name_en')
PROPERTY_TYPE_TYPES = PropertyTypeType.objects.all().values_list(
    'id', 'property_type_id', 'name_fr', 'name_en'
)
COUNTRIES = [
    {'id': x[0], 'names': x[1:]}
    for x in Country.objects.all().values_list('id', 'name_en', 'name_fr')
]
REALTY_FIELDS = [x.attname for x in Realty._meta.fields if x.attname not in ['id']]
YEAR = str(timezone.now().year)[-2:]


class ObjModelPipeline(object):
    def process_item(self, item, spider):
        print('///// realty item process /////')
        if 'new_price' in item:
            return self.process_price_update(item, spider)
        else:
            return self.process_new_item(item, spider)

    def process_new_item(self, item, spider):
        print('/// process new item ///', item['url'])
        values_dict = {}
        for ptt in PROPERTY_TYPE_TYPES:
            if item['property_type'] in ptt:
                values_dict['property_type_type_id'] = ptt[0]
                values_dict['property_type_id'] = ptt[1]

        if not values_dict.get('property_type_id'):
            for pt in PROPERTY_TYPES:
                if item['property_type'] in pt:
                    values_dict['property_type_id'] = pt[0]

        if item.get('seller'):
            seller = Seller.objects.get_or_create(name=item['seller'])
            values_dict['seller_id'] = seller[0].id

        if item.get('country'):
            for c in COUNTRIES:
                if item['country'] in c['names']:
                    values_dict['country_id'] = c['id']

        if item.get('city') and values_dict.get('country_id'):
            city = City.objects.get_or_create(
                name=item['city'], country_id=values_dict['country_id']
            )
            values_dict['city_id'] = city[0].id

        for field in REALTY_FIELDS:
            value = item.get(field)
            if value:
                values_dict[field] = value

        realty = Realty.objects.create(**values_dict)
        item['id'] = realty.id
        return item


    def process_price_update(self, item, spider):
        print('/// price update ///')
        item['model_obj'].update_price(item['new_price'])
        raise DropItem('Price updated')


class StoreImgPipeline(ImagesPipeline):

    def file_path(self, request, response=None, info=None):
        print('/// file path ///')
        ## start of deprecation warning block (can be removed in the future)
        def _warn():
            from scrapy.exceptions import ScrapyDeprecationWarning
            import warnings
            warnings.warn('ImagesPipeline.image_key(url) and file_key(url) methods are deprecated, '
                          'please use file_path(request, response=None, info=None) instead',
                          category=ScrapyDeprecationWarning, stacklevel=1)

        # check if called from image_key or file_key with url as first argument
        if not isinstance(request, Request):
            _warn()
            url = request
        else:
            url = request.url

        # detect if file_key() or image_key() methods have been overridden
        if not hasattr(self.file_key, '_base'):
            _warn()
            return self.file_key(url)
        elif not hasattr(self.image_key, '_base'):
            _warn()
            return self.image_key(url)
        ## end of deprecation warning block

        image_guid = hashlib.sha1(to_bytes(url)).hexdigest()  # change to request.url after deprecation
        return 'realty-sc/%s/%s/%s/%s.jpg' % (YEAR, image_guid[:2], image_guid[2:4], image_guid)


class SaveImgModelPipeline(object):
    def process_item(self, item, spider):
        print('/// process img model save ///')
        for img in item.get('images', []):
            RealtyImage.objects.create(realty_id=item['id'], image=img['path'])
        return item
