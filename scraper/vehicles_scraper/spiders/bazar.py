# -*- coding: utf-8 -*-
import datetime, json, re, scrapy
from ..items import CarItem
from vehicles.models import Car


class BazarSpider(scrapy.Spider):
    name = 'bazar'
    allowed_domains = ['bazar.lu']
    start_urls = ['http://www.bazar.lu']

    def parse(self, response):
        print('///start parsing///')
        return scrapy.Request(
            'http://www.bazar.lu/Scripts/sql.exe?SqlDB=bazar&Sql=Search.phs&category=2',
            callback=self.go_to_list
        )

    def go_to_list(self, response):
        return scrapy.FormRequest.from_response(response,
            formxpath='//form[@id="search_form"]',
            formdata={'type_annonce_offre':'on'},
            callback=self.parse_list
        )

    def parse_list(self, response):
        adv_list = response.css('div#search_result_content>div.item')
        for adv in adv_list:
            link = adv.xpath('./a/@href').extract_first()
            url = response.urljoin(link)
            car = Car.objects.filter(url=url).only('price').first()
            if not car:
                yield scrapy.Request(url, callback=self.parse_detail)
            else:
                print('///already exists///')
                price = adv.xpath(
                    './/div[@class="search_result_entry_info_price"]/text()[2]'
                ).extract_first().strip()
                if price:
                    price = int(price)
                    if price != car.price:
                        item['model_obj'] = car
                        item['new_price'] = price
                        yield item

        current_page_btn = response.xpath('//div[@class="page_link selectedPage"]')
        next_page_btn = current_page_btn.xpath('./following-sibling::div[1]')
        if next_page_btn.xpath('./@class').extract_first().strip() == 'page_link':
            next_page = next_page_btn.xpath('./div/text()').extract_first().strip()
            print('//////////////////////go to next page////////////////////', next_page)
            yield scrapy.FormRequest.from_response(response,
                formxpath='//form[@id="search_form"]',
                formdata={'page': next_page},
                callback=self.parse_list
            )


    def parse_detail(self, response):
        item = CarItem()
        item['url'] = response.url
        item['seller'] = {}

        price = response.xpath('//span[@id="annonce_price_highlight"]/text()').extract_first()
        price = price.strip() if price else ''
        country = response.xpath('//div[@id="annonce_address_country"]/text()').extract_first()
        country = country.strip() if country else ''
        address = response.xpath('//div[@id="annonce_address_street"]/text()').extract_first()
        address = address.strip() if address else ''
        city = response.xpath('//div[@id="annonce_address_city"]/text()').extract_first()
        city = city.strip() if city else ''
        if price:
            item['price'] = price
        if country:
            item['country'] = country.strip()
        if address and city:
            item['seller']['location'] = '%s, %s' % (address, city)
        elif address or city:
            item['seller']['location'] = address or city


        detail_block = response.xpath('//div[@class="annonce_attributs"]')
        selector = './/div[@id="annonce_attribute_%s"]//span/text()'
        details_map = (('brand', 71,),
                       ('model', 85),
                       ('model_type', 72),
                       ('gear_box', 73),
                       ('power', 74),
                       ('cylinder', 75),
                       ('km', 77),
                       ('energy', 79),
                       ('color', 80),
                       ('color_inside', 81),
                       ('places', 116),)

        for d in details_map:
            value = detail_block.xpath(selector % d[1]).extract_first()
            value = value.strip() if value else ''
            if value and value != '0':
                item[d[0]] = value
        if not item.get('brand'):
            return

        doors = detail_block.xpath(selector % 76).extract_first()
        year_month = detail_block.xpath(selector % 83).extract_first()
        if doors:
            item['doors'] = doors.strip()[-1:]
        if year_month:
            year_month = year_month.strip()
            if re.match(r'^\d{4}.\d{2}.*$', year_month):
                item['model_year'] = year_month[:4]
                item['model_month'] = year_month[5:7]
            elif re.match(r'^.*\d{2}.\d{4}$', year_month):
                item['model_year'] = year_month[-4:]
                item['model_month'] = year_month[-7:-5]

        options = detail_block.xpath(
            './/div[@class="annonce_attribute_list_entry"]/text()'
        ).extract()
        if options:
            item['options'] = json.dumps([x.strip() for x in options])
        description = response.xpath(
            '//div[@class="annonce_entry_description_long"]/text()'
        ).extract_first()
        if description:
            item['description'] = description.strip()

        img_block = response.css("div.annonce_entry_images")
        img_links = img_block.xpath('.//figure/a/@href').extract()
        if img_links:
            item['image_urls'] = [response.urljoin(x) for x in img_links]

        yield item

