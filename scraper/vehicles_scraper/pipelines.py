# -*- coding: utf-8 -*-

# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: http://doc.scrapy.org/en/latest/topics/item-pipeline.html

import hashlib
from django.db.models import Q
from django.utils import timezone
from scrapy.exceptions import DropItem
from scrapy.http import Request
from scrapy.pipelines.images import ImagesPipeline
from scrapy.utils.python import to_bytes

from vehicles.models import Car, CarImage, Brand, Model, Seller, Country, Color, BaseColor


BASE_COLORS = BaseColor.objects.all().values_list('id', 'color_en', 'color_fr')
COUNTRIES = [
    {'id': x[0], 'names': x[1:]}
    for x in Country.objects.all().values_list('id', 'name_en', 'name_fr')
]
CAR_FIELDS = [x for x in Car().__dict__.keys() if x not in ['_state', 'id']]
YEAR = str(timezone.now().year)[-2:]


class ObjModelPipeline(object):
    def process_item(self, item, spider):
        if item.get('new_price'):
            return self.process_price_update(item, spider)
        else:
            return self.process_new_item(item, spider)

    def process_new_item(self, item, spider):
        print('///', 'process new item')
        brand, created = Brand.objects.get_or_create(brand_name=item['brand'])
        seller = Seller.objects.create(**item['seller'])
        values_dict = {'seller_id': seller.id, 'brand_id': brand.id}

        if item.get('model'):
            car_model, created = Model.objects.get_or_create(
                model_name=item['model'], brand=brand
            )
            values_dict['model_id'] = car_model.id

        for key in ['color', 'color_inside']:
            if item.get(key):
                item[key] = item[key].lower()
                color = Color.objects.filter(Q(color_fr=item[key]) | Q(color_en=item[key])).first()
                if not color:
                    color_data = {'color': item[key]}
                    for c in BASE_COLORS:
                        if c[2] in item[key] or c[1] in item[key]:
                            color_data['base_color_id'] = c[0]
                            break
                    color = Color.objects.create(**color_data)
                values_dict['%s_id' % key] = color.id

        if item.get('country'):
            for c in COUNTRIES:
                if item['country'] in c['names']:
                    values_dict['country_id'] = c['id']
                    break

        for field in CAR_FIELDS:
            value = item.get(field)
            if value:
                values_dict[field] = value

        car = Car.objects.create(**values_dict)
        item['id'] = car.id
        return item

    def process_price_update(self, item, spider):
        print('/// process price update')
        item['model_obj'].update_price(item['new_price'])
        raise DropItem('Price updated')


class StoreImgPipeline(ImagesPipeline):

    def file_path(self, request, response=None, info=None):
        print('/// file path ///')
        ## start of deprecation warning block (can be removed in the future)
        def _warn():
            from scrapy.exceptions import ScrapyDeprecationWarning
            import warnings
            warnings.warn('ImagesPipeline.image_key(url) and file_key(url) methods are deprecated, '
                          'please use file_path(request, response=None, info=None) instead',
                          category=ScrapyDeprecationWarning, stacklevel=1)

        # check if called from image_key or file_key with url as first argument
        if not isinstance(request, Request):
            _warn()
            url = request
        else:
            url = request.url

        # detect if file_key() or image_key() methods have been overridden
        if not hasattr(self.file_key, '_base'):
            _warn()
            return self.file_key(url)
        elif not hasattr(self.image_key, '_base'):
            _warn()
            return self.image_key(url)
        ## end of deprecation warning block

        image_guid = hashlib.sha1(to_bytes(url)).hexdigest()  # change to request.url after deprecation
        return 'vehicles-sc/%s/%s/%s/%s.jpg' % (YEAR, image_guid[:2], image_guid[2:4], image_guid)


class SaveImgModelPipeline(object):
    def process_item(self, item, spider):
        print('/// process img model save ///')
        for img in item.get('images', []):
            CarImage.objects.create(car_id=item['id'], image=img['path'])
        return item
