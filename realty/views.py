# -*- coding: utf-8 -*-
import copy, urllib
import simplejson as json
from django.views.generic import ListView, TemplateView, DetailView
from django.http import HttpResponseRedirect
from django.http.response import HttpResponse
from django.shortcuts import render
from django_ajax.mixin import AJAXMixin
from geoip.utils import get_location_by_ip
from haystack.query import SearchQuerySet
from haystack.utils.geo import Point, D

from realty.form import RealtyForm, RealtyFormset
from .models import Realty, RealtyImage, PropertyType, PropertyTypeType, City
from vehicles.models import Country

energy_class = Realty.ENERGY_CHOICES
property_types = list(PropertyType.objects.all())
cities = []
property_type_types = list(PropertyTypeType.objects.all())
property_tt_dict = {x.id: x for x in property_type_types}


class SearchView(ListView):
    template_name = 'realty/realty_search.html'
    queryset = Realty.objects.none()

    def get_context_data(self, **kwargs):
        context = super(SearchView, self).get_context_data()
        context['location'] = get_location_by_ip(self.request)
        context['list_of_country'] = Country.objects.all()
        return context


class DetailRealtyView(DetailView):
    model = Realty


class AddRealtyView(TemplateView):
    template_name = 'realty/add_realty.html'
    realty_form = RealtyForm()
    formset = RealtyFormset()
    success_url = '/'

    def get(self, request, *args, **kwargs):
        realty_form = RealtyForm()
        formset = RealtyFormset()
        return render(request, 'realty/add_realty.html', {"realty_form": realty_form, "formset": formset})

    def post(self, request):
        print('post')
        realty_form = RealtyForm(data=request.POST)
        formset = RealtyFormset(data=request.POST, files=request.FILES, queryset=RealtyImage.objects.none())
        if realty_form.is_valid() and formset.is_valid():
            print('form valid')
            return self.form_valid(realty_form, formset, request)
        print('form not valid')
        print(realty_form.errors)
        return render(request, self.template_name, {"realty_form": realty_form, "formset": formset})

    def form_valid(self, realty_form, formset, request):
        city_name = realty_form['city']
        country = Country.objects.get(pk=realty_form['country'].value())
        city = City.objects.get_or_create(name=city_name.value(), country=country)
        realty = realty_form.save(commit=False)
        realty.city = city[0]
        realty.save()
        for form in formset:
            data = form.cleaned_data
            image = data.get('image')
            if image > 0:
                photo = RealtyImage(realty=Realty.objects.get(pk=realty.id), image=image)
                photo.save()
        return HttpResponseRedirect(realty.get_absolute_url())


def get_city(request):
    if request.is_ajax():
        q = request.GET.get('term', '')
        foreign_key = request.GET.get('id_country', '')
        objects = City.objects.filter(name__istartswith=q)[:20]
        results = []
        for object in objects:
            if foreign_key == "" or object.country_id == int(foreign_key):
                object_json = {}
                object_json['city'] = object.name
                results.append(object_json['city'])
        data = json.dumps(results)
        return HttpResponse(data)
    else:
        return HttpResponse(status=403)


class ElasticSearchAjax(AJAXMixin, TemplateView):
    FACET = [
        # 'country',
        'energy_class',
        'property_type_type',
    ]

    RANGE_FILTER = [
        'year_index',
        'price_index',
    ]

    def __init__(self):
        self.q = SearchQuerySet(using='realty')

    def queryset(self):
        q = self.q
        r = self.request.GET

        if r.get('q'):
            q = q.filter(text=r.get('q'))

        for p in r:
            if p in self.FACET:
                params = {p: r.get(p)}
                q = q.filter(**params)
            elif p in self.RANGE_FILTER:
                y = r.get(p).split(',')

                params = {p + '__range': [int(y[0].replace('.', '')), int(y[1].replace('.', ''))]}
                q = q.filter(**params)

        property_type = r.get('property_type')
        if property_type:
            q = q.filter(property_type=property_type)

        city = r.get('city')
        if city:
            q = q.filter(city=city)

        country = r.get('countries')
        global cities
        if country:
            cities = []
            q = q.filter(country__in=country.split(','))
            for country in country.split(','):
                country = Country.objects.get(pk=country)
                for city in (City.objects.filter(country=country)):
                    cities.append(city)
        else:
            cities = list(City.objects.all())

        deal = r.get('deal')
        if deal:
            q = q.filter(deal=deal)

        energy_class = r.get('energy_class')
        if energy_class:
            q = q.filter(energy_class=energy_class)

        distance = r.get('distance')
        if distance and distance != '1000.00':
            distance = D(km=int(r.get('distance').replace('.00', '')))

            if 'location' in self.request.COOKIES:
                location = json.loads(urllib.unquote(self.request.COOKIES['location']))
                latitude = location['latitude']
                longitude = location['longitude']
            else:
                location = get_location_by_ip(self.request)
                latitude = location.location.latitude
                longitude = location.location.longitude
            if location:
                user_point = Point(float(longitude), float(latitude))
                q = q.dwithin('location_index', user_point, distance)

        sort = r.get('sort')
        if sort:
            q = q.order_by(sort)

        return q

    def faceted(self, search_result):
        for f in self.FACET:
            search_result = search_result.facet(f)
        return search_result.facet_counts()

    def check_boxes_filters(self, facets):
        check_boxes_filters = copy.deepcopy(facets)
        if 'country' in check_boxes_filters.keys():
            del check_boxes_filters["country"]
        if 'energy_class' in check_boxes_filters.keys():
            del check_boxes_filters["energy_class"]
        return check_boxes_filters

    def check_boxes_filters_energy(self, facets):
        check_boxes_filters = copy.deepcopy(facets)
        if 'country' in check_boxes_filters.keys():
            del check_boxes_filters["country"]
        if 'property_type_type' in check_boxes_filters.keys():
            del check_boxes_filters["property_type_type"]
        return check_boxes_filters

    def get(self, request, *args, **kwargs):
        search_result = self.queryset()

        facets = self.faceted(search_result)['fields']

        search_list_result = lambda: render(
            request,
            'realty/item_preview.html',
            {'query': True, 'object_list': search_result}
        )

        left_sidebar_filters = lambda: render(
            request,
            'realty/left_sidebar_filters.html',
            {
                'facets': self.check_boxes_filters(facets),
                'conditions': request.GET,
                'cities': cities,
                'energy_class': sorted(self.check_boxes_filters_energy(facets)['energy_class']),
                'property_types': property_types,
                'property_type_types': property_type_types,
                'selected_pt': int(request.GET.get('property_type', 0)),
                'selected_city': int(request.GET.get('city', 0)),
                'property_tt_dict': property_tt_dict,
            }
        )
        if not request.GET.get('scroll'):
            AJAX_TEMPLATE = {
                'inner-fragments': {'#search-list-result': search_list_result(),
                                    '#left-sidebar-filters': left_sidebar_filters(), }
            }
        else:
            AJAX_TEMPLATE = {
                'append-fragments': {'#search-list-result': search_list_result(), }
            }

        return AJAX_TEMPLATE
