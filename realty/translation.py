# -*- coding: utf-8 -*-
from modeltranslation.translator import register, TranslationOptions

from .models import PropertyType, PropertyTypeType


@register(PropertyType)
class PropertyTypeTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(PropertyTypeType)
class PropertyTypeTypeTranslationOptions(TranslationOptions):
    fields = ('name',)
