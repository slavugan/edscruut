# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('realty', '0002_auto_20160719_1429'),
    ]

    operations = [
        migrations.AddField(
            model_name='realty',
            name='land_area',
            field=models.PositiveIntegerField(null=True),
        ),
    ]
