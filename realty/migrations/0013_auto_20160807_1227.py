# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('realty', '0012_auto_20160804_1356'),
    ]

    operations = [
        migrations.RenameField(
            model_name='realty',
            old_name='year_build',
            new_name='year_built',
        ),
    ]
