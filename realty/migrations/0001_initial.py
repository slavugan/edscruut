# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.contrib.postgres.fields
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0017_car_weight'),
    ]

    operations = [
        migrations.CreateModel(
            name='City',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('country', models.ForeignKey(to='vehicles.Country')),
            ],
        ),
        migrations.CreateModel(
            name='PropertyType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('name_en', models.CharField(max_length=50, null=True)),
                ('name_fr', models.CharField(max_length=50, null=True)),
            ],
        ),
        migrations.CreateModel(
            name='PropertyTypeType',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=50)),
                ('name_en', models.CharField(max_length=50, null=True)),
                ('name_fr', models.CharField(max_length=50, null=True)),
                ('property_type', models.ForeignKey(to='realty.PropertyType')),
            ],
        ),
        migrations.CreateModel(
            name='Realty',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('deal', models.PositiveIntegerField(choices=[(1, b'sale'), (2, b'rent')])),
                ('rooms', models.PositiveIntegerField(null=True)),
                ('area', models.PositiveIntegerField(null=True)),
                ('price', models.PositiveIntegerField(null=True)),
                ('price_history', django.contrib.postgres.fields.ArrayField(size=None, null=True, base_field=models.IntegerField(), blank=True)),
                ('energy_class', models.CharField(max_length=1, choices=[(b'A', b'A'), (b'B', b'B'), (b'C', b'C'), (b'D', b'D'), (b'E', b'E'), (b'F', b'F'), (b'G', b'G'), (b'H', b'H'), (b'I', b'I')])),
                ('termal_protection_class', models.CharField(max_length=1, choices=[(b'A', b'A'), (b'B', b'B'), (b'C', b'C'), (b'D', b'D'), (b'E', b'E'), (b'F', b'F'), (b'G', b'G'), (b'H', b'H'), (b'I', b'I')])),
                ('latitude', models.FloatField(null=True, blank=True)),
                ('longitude', models.FloatField(null=True, blank=True)),
                ('year_build', models.PositiveIntegerField(null=True, blank=True)),
                ('new_building', models.NullBooleanField()),
                ('url', models.CharField(max_length=300, null=True, blank=True)),
                ('has_child', models.NullBooleanField()),
                ('city', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, blank=True, to='realty.City', null=True)),
                ('country', models.ForeignKey(to='vehicles.Country', on_delete=django.db.models.deletion.PROTECT)),
                ('part_of', models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, to='realty.Realty', null=True)),
                ('property_type', models.ForeignKey(to='realty.PropertyType', on_delete=django.db.models.deletion.PROTECT)),
                ('property_type_type', models.ForeignKey(on_delete=django.db.models.deletion.PROTECT, blank=True, to='realty.PropertyTypeType', null=True)),
            ],
        ),
    ]
