# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import realty.models


class Migration(migrations.Migration):

    dependencies = [
        ('realty', '0005_auto_20160726_1518'),
    ]

    operations = [
        migrations.CreateModel(
            name='RealtyImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(upload_to=b'', verbose_name=realty.models.img_upload_to)),
                ('realty', models.ForeignKey(to='realty.Realty')),
            ],
        ),
    ]
