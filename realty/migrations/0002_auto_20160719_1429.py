# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('realty', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='realty',
            name='description',
            field=models.TextField(blank=True),
        ),
        migrations.AddField(
            model_name='realty',
            name='floor',
            field=models.PositiveIntegerField(null=True),
        ),
        migrations.AddField(
            model_name='realty',
            name='total_floors',
            field=models.PositiveIntegerField(null=True),
        ),
    ]
