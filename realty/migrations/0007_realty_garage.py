# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('realty', '0006_realtyimage'),
    ]

    operations = [
        migrations.AddField(
            model_name='realty',
            name='garage',
            field=models.PositiveIntegerField(null=True),
        ),
    ]
