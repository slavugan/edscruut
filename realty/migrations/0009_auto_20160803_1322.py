# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion
import django.contrib.postgres.fields
import django.core.validators


class Migration(migrations.Migration):

    dependencies = [
        ('realty', '0008_auto_20160727_1025'),
    ]

    operations = [
        migrations.AlterField(
            model_name='realty',
            name='deal',
            field=models.PositiveIntegerField(default=1, choices=[(1, b'sale'), (2, b'rent')]),
        ),
        migrations.AlterField(
            model_name='realty',
            name='phones',
            field=django.contrib.postgres.fields.ArrayField(null=True, base_field=models.CharField(max_length=24, validators=[django.core.validators.RegexValidator(b'^[\\s \\+ 0-9]{6,17}$', b'Enter a valid phone number.')]), size=None),
        ),
        migrations.AlterField(
            model_name='realty',
            name='seller',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='realty.Seller', null=True),
        ),
        migrations.AlterField(
            model_name='realty',
            name='year_build',
            field=models.PositiveIntegerField(blank=True, null=True, validators=[django.core.validators.MaxValueValidator(2016), django.core.validators.MinValueValidator(1800)]),
        ),
    ]
