# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.contrib.postgres.fields
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('realty', '0004_auto_20160725_1031'),
    ]

    operations = [
        migrations.CreateModel(
            name='Seller',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=60, null=True)),
            ],
        ),
        migrations.AddField(
            model_name='realty',
            name='address',
            field=models.CharField(max_length=100, null=True),
        ),
        migrations.AddField(
            model_name='realty',
            name='balcony',
            field=models.PositiveIntegerField(null=True),
        ),
        migrations.AddField(
            model_name='realty',
            name='bathroom',
            field=models.PositiveIntegerField(null=True),
        ),
        migrations.AddField(
            model_name='realty',
            name='garden',
            field=models.DecimalField(null=True, max_digits=5, decimal_places=2),
        ),
        migrations.AddField(
            model_name='realty',
            name='phones',
            field=django.contrib.postgres.fields.ArrayField(null=True, base_field=models.CharField(max_length=100), size=None),
        ),
        migrations.AddField(
            model_name='realty',
            name='terrace',
            field=models.PositiveIntegerField(null=True),
        ),
        migrations.AddField(
            model_name='realty',
            name='seller',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, to='realty.Seller', null=True),
        ),
    ]
