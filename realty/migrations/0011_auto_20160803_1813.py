# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('realty', '0010_auto_20160803_1552'),
    ]

    operations = [
        migrations.AlterField(
            model_name='realty',
            name='address',
            field=models.CharField(max_length=100, null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='realty',
            name='area',
            field=models.PositiveIntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='realty',
            name='balcony',
            field=models.PositiveIntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='realty',
            name='bathroom',
            field=models.PositiveIntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='realty',
            name='energy_class',
            field=models.CharField(blank=True, max_length=1, choices=[(b'A', b'A'), (b'B', b'B'), (b'C', b'C'), (b'D', b'D'), (b'E', b'E'), (b'F', b'F'), (b'G', b'G'), (b'H', b'H'), (b'I', b'I')]),
        ),
        migrations.AlterField(
            model_name='realty',
            name='floor',
            field=models.PositiveIntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='realty',
            name='garage',
            field=models.PositiveIntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='realty',
            name='garden',
            field=models.DecimalField(null=True, max_digits=5, decimal_places=2, blank=True),
        ),
        migrations.AlterField(
            model_name='realty',
            name='land_area',
            field=models.DecimalField(null=True, max_digits=7, decimal_places=2, blank=True),
        ),
        migrations.AlterField(
            model_name='realty',
            name='part_of',
            field=models.ForeignKey(on_delete=django.db.models.deletion.SET_NULL, blank=True, to='realty.Realty', null=True),
        ),
        migrations.AlterField(
            model_name='realty',
            name='price',
            field=models.PositiveIntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='realty',
            name='rooms',
            field=models.PositiveIntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='realty',
            name='termal_protection_class',
            field=models.CharField(blank=True, max_length=1, choices=[(b'A', b'A'), (b'B', b'B'), (b'C', b'C'), (b'D', b'D'), (b'E', b'E'), (b'F', b'F'), (b'G', b'G'), (b'H', b'H'), (b'I', b'I')]),
        ),
        migrations.AlterField(
            model_name='realty',
            name='terrace',
            field=models.PositiveIntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='realty',
            name='total_floors',
            field=models.PositiveIntegerField(null=True, blank=True),
        ),
    ]
