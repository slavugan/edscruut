from django import forms
from django.utils.encoding import force_text
from django.utils.html import format_html
from django.utils.safestring import mark_safe

from realty.models import Realty, RealtyImage, PropertyTypeType, City
from django.forms import inlineformset_factory
from django.forms import ModelForm, TextInput, Select, Textarea, NumberInput, \
    FileInput, URLInput, DateInput, CheckboxInput, RadioSelect


class SelectPropertyTypeType(forms.Select):
    def render_option(self, selected_choices, option_value, option_label):
        name = ''
        if option_value is None:
            option_value = ''
        option_value = force_text(option_value)
        if option_value in selected_choices:
            selected_html = mark_safe(' selected="selected"')
            if not self.allow_multiple_selected:
                selected_choices.remove(option_value)
        else:
            selected_html = ''
        if option_value != '':
            name = "PropertyTypeType" + str(
                PropertyTypeType.objects.get(pk=option_value).property_type_id
            )
        return format_html('<option  name="{}" value="{}"{}>{}</option>',
                           name,
                           option_value,
                           selected_html,
                           force_text(option_label))


class RealtyForm(forms.ModelForm):
    city = forms.CharField(required=True,
                           widget=forms.TextInput(
                               attrs={'class': 'form-control  c-square c-theme', 'title': 'Country'}))

    class Meta:
        model = Realty
        exclude = ('latitude', 'url', 'longitude', 'part_of', 'has_child', 'seller', 'city', 'creation_date')
        widgets = {
            'property_type': Select(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Property type'}),
            'property_type_type': SelectPropertyTypeType(
                attrs={'class': 'form-control  c-square c-theme', "autocomplete": "on"}),
            'deal': RadioSelect(
                attrs={'class': 'c-radio', 'title': 'Deal'}),
            'rooms': NumberInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Rooms'}),
            'area': NumberInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Area'}),
            'land_area': NumberInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Land area'}),
            'price': NumberInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Price'}),
            'energy_class': Select(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Energy class'}),
            'termal_protection_class': Select(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Termal protection class'}),
            'country': Select(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Country'}),
            'address': TextInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Address'}),
            'year_built': NumberInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Year built'}),
            'new_building': CheckboxInput(
                attrs={'class': 'c-check', 'id': "checkbox1-77", 'title': 'New building'}),
            'floor': NumberInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Floor'}),
            'total_floors': NumberInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Total floors'}),
            'description': Textarea(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Description'}),
            'bathroom': NumberInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Bathroom'}),
            'garden': NumberInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Garden'}),
            'terrace': NumberInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Terrace'}),
            'balcony': NumberInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Balcony'}),
            'garage': NumberInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Garage'}),
            'phones': TextInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Phones'}),
        }


RealtyFormset = inlineformset_factory(
    Realty,
    RealtyImage,
    fields=['image'],
    extra=30,
    widgets={'image': FileInput(attrs={
        'class': 'form-control  c-square c-theme', 'style': 'Display: none;', 'onchange': 'hide()'
    })}
)
