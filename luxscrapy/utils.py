import re

def str_to_int(str):
    return re.sub("[^0-9]", "", str)