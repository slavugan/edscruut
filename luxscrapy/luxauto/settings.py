# -*- coding: utf-8 -*-

# Scrapy settings for luxauto project
#
# For simplicity, this file contains only settings considered important or
# commonly used. You can find more settings consulting the documentation:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#     http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html
#     http://scrapy.readthedocs.org/en/latest/topics/spider-middleware.html

BOT_NAME = 'luxauto'

SPIDER_MODULES = ['luxauto.spiders']
NEWSPIDER_MODULE = 'luxauto.spiders'


# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'luxauto (+http://www.yourdomain.com)'
USER_AGENT_LIST = [
    'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.7 (KHTML, like Gecko) Chrome/16.0.912.36 Safari/535.7',
    'Mozilla/5.0 (Windows NT 6.2; Win64; x64; rv:16.0) Gecko/16.0 Firefox/16.0',
    'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/534.55.3 (KHTML, like Gecko) Version/5.1.3 Safari/534.53.10'
]

# Configure maximum concurrent requests performed by Scrapy (default: 16)
CONCURRENT_REQUESTS=6

# Configure a delay for requests for the same website (default: 0)
# See http://scrapy.readthedocs.org/en/latest/topics/settings.html#download-delay
# See also autothrottle settings and docs
RANDOMIZE_DOWNLOAD_DELAY=True
# The download delay setting will honor only one of:
CONCURRENT_REQUESTS_PER_DOMAIN=3
#CONCURRENT_REQUESTS_PER_IP=16

# Disable cookies (enabled by default)
COOKIES_ENABLED=False

# Disable Telnet Console (enabled by default)
#TELNETCONSOLE_ENABLED=False

# Override the default request headers:
#DEFAULT_REQUEST_HEADERS = {
#   'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,*/*;q=0.8',
#   'Accept-Language': 'en',
#}

# Enable or disable spider middlewares
# See http://scrapy.readthedocs.org/en/latest/topics/spider-middleware.html
#SPIDER_MIDDLEWARES = {
#    'luxauto.middlewares.MyCustomSpiderMiddleware': 543,
#}

# Enable or disable downloader middlewares
# See http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html
#DOWNLOADER_MIDDLEWARES = {
#    'luxauto.middlewares.MyCustomDownloaderMiddleware': 543,
#}

# Enable or disable extensions
# See http://scrapy.readthedocs.org/en/latest/topics/extensions.html
#EXTENSIONS = {
#    'scrapy.telnet.TelnetConsole': None,
#}

# Configure item pipelines
# See http://scrapy.readthedocs.org/en/latest/topics/item-pipeline.html
ITEM_PIPELINES = {
   # 'luxauto.pipelines.SomePipeline': 300,
   'luxauto.pipelines.DjangoWriterPipeline': 300,
}


# Enable and configure the AutoThrottle extension (disabled by default)
# See http://doc.scrapy.org/en/latest/topics/autothrottle.html
# NOTE: AutoThrottle will honour the standard settings for concurrency and delay
#AUTOTHROTTLE_ENABLED=True
# The initial download delay
#AUTOTHROTTLE_START_DELAY=5
# The maximum download delay to be set in case of high latencies
#AUTOTHROTTLE_MAX_DELAY=60
# Enable showing throttling stats for every response received:
#AUTOTHROTTLE_DEBUG=False

# Enable and configure HTTP caching (disabled by default)
# See http://scrapy.readthedocs.org/en/latest/topics/downloader-middleware.html#httpcache-middleware-settings
#HTTPCACHE_ENABLED=True
#HTTPCACHE_EXPIRATION_SECS=0
#HTTPCACHE_DIR='httpcache'
#HTTPCACHE_IGNORE_HTTP_CODES=[]
#HTTPCACHE_STORAGE='scrapy.extensions.httpcache.FilesystemCacheStorage'

ITEM_PIPELINES = {'luxauto.pipelines.MyImagesPipeline': 1}
#ITEM_PIPELINES = {'scrapy.contrib.pipeline.images.ImagesPipeline': 1}
AWS_ACCESS_KEY_ID = "AKIAJ4BIIZEXZEWSLZ6A"
AWS_SECRET_ACCESS_KEY = "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"
# IMAGES_STORE = "s3://bucketname/base-key-dir-if-any/"
IMAGES_STORE = "s3://luxautos/"
# IMAGES_STORE = "s3://s3-website.eu-central-1.amazonaws.com/luxauto"

IMAGES_EXPIRES = 180 # The amount of days until we re-download the image

IMAGES_THUMBS = {
    'small': (50, 50), # You can add as many of these as you want
    'big': (300, 300)
}


# export DATABASE_URL="postgres://root:''@127.0.0.1:5432/luxdjango"

# def setup_django_env(path):
#     import imp, os
#     from django.core.management import setup_environ

#     f, filename, desc = imp.find_module('settings', [path])
#     project = imp.load_module('settings', f, filename, desc)       

#     setup_environ(project)

# setup_django_env('/root/projects/luxdjango')

import os, sys
import django



PROJECT_ROOT = os.path.abspath(os.path.dirname(__file__))
sys.path.insert(0, os.path.join(PROJECT_ROOT, "../..")) #only for example_project
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "luxdjango.settings.production")

if os.environ.has_key('DEVELOPMENT'):
    os.environ.setdefault("DJANGO_SETTINGS_MODULE", "luxdjango.settings.local")

django.setup()
# if os.environ.get('DEVELOPMENT', None):
# 	print "here"
# from settings_dev import *


# if settings.DEVELOPMENT:
	# print "local settings"
# print settings.LOCAL
# if settings.LOCAL:
# 	os.environ.setdefault("DJANGO_SETTINGS_MODULE", "luxdjango.settings.local")
# os.environ.setdefault("DJANGO_SETTINGS_MODULE", "myapp.settings")
# from vehicles import models


# print models.Brand.objects.all()