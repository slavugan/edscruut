# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class LuxautoItem(scrapy.Item):
    # define the fields for your item here like:
    url = scrapy.Field()
    scraped_time = scrapy.Field()
    marque = scrapy.Field()
    model = scrapy.Field()
    type = scrapy.Field()
    finition = scrapy.Field()
    seller = scrapy.Field()
    contact = scrapy.Field()
    location = scrapy.Field()
    year = scrapy.Field()
    km = scrapy.Field()
    price = scrapy.Field()
    energy = scrapy.Field()
    gear_box = scrapy.Field()
    cylinder = scrapy.Field()
    power = scrapy.Field()
    color = scrapy.Field()
    color_inside = scrapy.Field()
    doors = scrapy.Field()
    premier_main = scrapy.Field()
    image_urls = scrapy.Field()
    images = scrapy.Field()



