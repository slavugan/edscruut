# -*- coding: utf-8 -*-
import re
from datetime import datetime
import scrapy
from scrapy.selector import  Selector
from scrapy.http import Request
from scrapy.utils.response import get_base_url
from scrapy.utils.url import urljoin_rfc
from luxscrapy.luxauto.items import LuxautoItem
from luxscrapy.utils import str_to_int
from vehicles.models import Vehicle, Brand, Model, Car, Seller


class LuxSpider(scrapy.Spider):
    name = "luxspider"
    allowed_domains = ["luxauto.lu"]
    start_urls = (
        # 'http://www.luxauto.lu/',
        'http://www.luxauto.lu/fr/occasions/search/?v_id=1&marque=&modele=&carburant%5B%5D=&type_compte=&prixde=&prixa=&kmde=&kma=&anneede=&anneea=&date_introduction=&search_accueil=%3E%3E+rechercher',
    )

    def parse(self, response):
        # b = Brand(brand_name='Checking',brand_code='12345')        
        # b.save()
        # b = Brand()
        # print b.objects.all()

        hxs = Selector(response)
        links = hxs.xpath('//p[@class="marque"]/a/@href').extract()
        print links

        for link in links[::-1]:
            # continue
            yield Request(urljoin_rfc(get_base_url(response),link),self.parse_item)

        """Handling pagination"""
        next_page = hxs.xpath('//a[@title="Page suivante"]/@href').extract()
        if next_page:
            print next_page
            yield Request(urljoin_rfc(get_base_url(response),next_page[0]),self.parse)

    def parse_item(self, response):
        print response.url
        hxs = Selector(response)
        header = hxs.xpath('//h2[@class="titre_l"]//text()').extract()
        item = LuxautoItem()
        item['url'] = response.url
        item['scraped_time'] = datetime.now()
        if header:
            marque_and_model = header[0].split()
            item['marque'] = marque_and_model[0]
            item['model'] = marque_and_model[1]

            type_and_finition = header[1].strip().split()
            item['type'] = ' '.join(type_and_finition[:2])
            # if len(type_and_finition) >= 3 and isinstance(type_and_finition[-1],str):
            if len(type_and_finition) >= 3:
                item['finition'] = type_and_finition[-1]

        seller = hxs.xpath('normalize-space(//p[@class="statut_vendeur"]/text())').extract()
        if seller:
            item['seller'] = seller[0]

        contact = hxs.xpath('//dl[@class="renseignements_num"]/dd/text()').extract()
        if contact:
            item['contact'] = ','.join([re.search('\d+.*',el).group() for el in contact])

        address = hxs.xpath('//div[@class="renseignements_adresse"]/p/text()').extract()
        if address:
            item['location'] = ' '.join(address)

        description_left = hxs.xpath('//dl[@class="desc_l"]')

        year = description_left.xpath(u'./dt[text()="Année"]/following-sibling::dd[1]/text()').extract()
        if year:
            item['year'] = year

        km = description_left.xpath('./dt[text()="km"]/following-sibling::dd[1]/text()').extract()
        if km:
            item['km'] = str_to_int(km[0].strip(':'))

        price = description_left.xpath('./dt[text()="Prix"]/following-sibling::dd[1]/text()').extract()
        if price:
            item['price'] = str_to_int(price[0].strip(':'))

        energy = description_left.xpath('./dt[text()="Carburant"]/following-sibling::dd[1]/text()').extract()
        if energy:
            item['energy'] = energy[0].strip(':')


        gear_box = description_left.xpath(u'./dt[text()="Boîte"]/following-sibling::dd[1]/text()').extract()
        if gear_box:
            item['gear_box'] = gear_box[0].strip(':')

        cylinder = description_left.xpath(u'./dt[text()="Cylindrée"]/following-sibling::dd[1]/text()').extract()
        if cylinder:
            item['cylinder'] = cylinder[0].strip(':')

        power = description_left.xpath('./dt[text()="Puissance"]/following-sibling::dd[1]/text()').extract()
        if power:
            item['power'] = power[0].strip(':')

        
        description_right = hxs.xpath('//div[@class="desc_r"]/dl')
        # color = description_right.xpath('./dt[text()="Couleur ext."]/following-sibling::dd[1]/text()').extract()
        # if color:
        #     item['color'] = color[0].strip(':')

        color_inside = description_right.xpath('./dt[text()="Couleur int."]/following-sibling::dd[1]/text()').extract()
        if color_inside:
            item['color_inside'] = color_inside[0].strip(':')

        doors = description_right.xpath('./dt[text()="Portes"]/following-sibling::dd[1]/text()').extract()
        if doors:
            item['doors'] = doors[0].strip(':')

        images_urls = hxs.xpath('//div[@class="cadre_agrandissement_fond"]/a/@href').extract()
        if images_urls:
            item['image_urls'] = images_urls

        try:
            try:
                brand, created = Brand.objects.get_or_create(brand_name=item['marque'])
            except Exception as e:
                print e.message
                
            try:
                model, created = Model.objects.get_or_create(brand=brand,model_name=item['model'])
            except Exception as e:
                print e.message

            """INserting data in cars table """
            model_month_and_year = item['year'][0].strip(':').split('-')
            model_month = int(model_month_and_year[0])
            model_year = int(model_month_and_year[1])

            try:
                vehicle, created = Vehicle.objects.get_or_create(of_type='car')
            except Exception as e:
                vehicle = None

            try:
                s, created = Seller.objects.get_or_create(
                    seller=item['seller'],
                    contact=item['contact'],
                    location=item['location']
                )
                Car.objects.filter(url=item['url']).update(seller=s.id)
            except Exception as e:
                print e.message

            car, created = Car.objects.get_or_create(
                model=model,
                vehicle_type=vehicle,
                brand=brand,
                model_type=item['type'],
                model_month=int(model_month),
                model_year=model_year,
                # seller=seller,
                km = item['km'],
                price = item['price'],
                energy = item['energy'],
                gear_box = item['gear_box'],
                cylinder=item['cylinder'],
                power=item['power'],
                # color=item['color'],
                color_inside=item['color_inside'],
                doors=int(item['doors']),
                # latitude= ,
                # longitude= ,
                url = item['url']
            )

        except Exception as e:
            print e.message

        print item
        yield item


