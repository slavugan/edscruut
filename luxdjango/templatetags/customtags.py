# -*- coding: utf-8 -*-
import re
from django import template

register = template.Library()

@register.assignment_tag(takes_context=True)
def get_url_first_arg(context):
    v = re.search(r'\w+', context.request.path_info)
    return v.group() if v else ''
