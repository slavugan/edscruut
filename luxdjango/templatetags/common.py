from django import template
from vehicles.models import Car

register = template.Library()


@register.filter()
def key(d, key_name):
    return d.get(key_name)

@register.filter()
def intkey(d, key):
    v = d.get(key)
    return int(v) if v else None

@register.assignment_tag()
def getbykey(d, key):
    return d.get(key)