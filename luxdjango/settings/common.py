# -*- coding: <utf-8> -*-
"""
Django settings for luxdjango project on Heroku. Fore more info, see:
https://github.com/heroku/heroku-django-template

For more information on this file, see
https://docs.djangoproject.com/en/1.9/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.9/ref/settings/
"""

from __future__ import absolute_import, unicode_literals

import os, sys
import dj_database_url

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
# from boto.gs.user import User

BASE_DIR = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
PROJECT_ROOT = os.path.join(BASE_DIR, 'luxdjango')

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.9/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = "axhrdne%6rjbv%%l#j*kvz=n+qlax8t3p(1y#n#1extp0xt_pi"

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

# Application definition

DJANGO_APPS = (
    'grappelli',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'modeltranslation',
    'django.contrib.admin',
    # 'django.contrib.gis',
)

THIRD_PARTY_APPS = (
    'crispy_forms',  # Form layouts
    'allauth',  # registration
    'allauth.account',  # registration
    'allauth.socialaccount',  # registration
    'allauth.socialaccount.providers.facebook',
    'allauth.socialaccount.providers.twitter',
    'allauth.socialaccount.providers.google',

    # sssss >
    # 'dynamic_scraper',
    # sssss <
    'djcelery',

    'haystack',
    'pagination',
    'django_ajax',
    'rosetta',
    'sorl.thumbnail',
)

# Apps specific for this project go here.
LOCAL_APPS = (
    'luxdjango.users',  # custom users app
    'luxdjango',
    # Your stuff: custom apps go here
    'vehicles',
    'realty',
    'scraper',
    'geoip',
)

# See: https://docs.djangoproject.com/en/dev/ref/settings/#installed-apps
INSTALLED_APPS = DJANGO_APPS + THIRD_PARTY_APPS + LOCAL_APPS

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'pagination.middleware.PaginationMiddleware',
)

ROOT_URLCONF = 'luxdjango.urls'

TEMPLATES = (
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(PROJECT_ROOT, 'templates'),
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
            'debug': DEBUG,
        },
    },
)

WSGI_APPLICATION = 'luxdjango.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite12'),
    }
}

AUTH_PASSWORD_VALIDATORS = (
    {'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',},
    {'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',},
    {'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',},
    {'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',},
)

# haystack

HAYSTACK_FUZZY_MIN_SIM = 0.5
HAYSTACK_FUZZY_MAX_EXPANSIONS = 50

# facebook

FACEBOOK_APP_ID = '846209455390520'
FACEBOOK_APP_SECRET = '0a2bcd13bc691786d6ad897cb06e31f6'

# Internationalization
# https://docs.djangoproject.com/en/1.9/topics/i18n/

LANGUAGE_CODE = 'en'
LANGUAGES = [
    ('en', 'English'),
    ('fr', 'French'),
]
LOCALE_PATHS = [
    os.path.join(BASE_DIR, 'luxdjango/locale'),
]
MODELTRANSLATION_FALLBACK_LANGUAGES = ('en', 'fr')
# export PYTHONPATH="${PYTHONPATH}:/path/to/project"
# django-admin makemessages -l fr -l en --settings=luxdjango.settings.dev -e html,py
# django-admin compilemessages --settings=luxdjango.settings.dev



TIME_ZONE = 'UTC'
USE_I18N = True
USE_L10N = True
USE_TZ = True

# Update database configuration with $DATABASE_URL.
# db_from_env = dj_database_url.config(conn_max_age=500)
# DATABASES['default'].update(db_from_env)

# Honor the 'X-Forwarded-Proto' header for request.is_secure()
SECURE_PROXY_SSL_HEADER = ('HTTP_X_FORWARDED_PROTO', 'https')

# Allow all host headers
ALLOWED_HOSTS = ['*']

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.9/howto/static-files/


# after collectstatic
STATIC_ROOT = os.path.join(PROJECT_ROOT, 'staticfiles')
STATIC_URL = '/static/'

MEDIA_ROOT = os.path.join(PROJECT_ROOT, 'media')
MEDIA_URL = '/media/'

# import sys
# sys.exit(1)

# Extra places for collectstatic to find static files.
STATICFILES_DIRS = [
    os.path.join(PROJECT_ROOT, 'static'),
]

# Simplified static file serving.
# https://warehouse.python.org/project/whitenoise/
# STATICFILES_STORAGE = 'whitenoise.django.GzipManifestStaticFilesStorage'

# Custom user app defaults
# Select the correct user model
AUTH_USER_MODEL = 'users.User'
LOGIN_REDIRECT_URL = 'users:redirect'
LOGIN_URL = 'account_login'
ACCOUNT_USERNAME_REQUIRED = False
ACCOUNT_USER_EMAIL_FIELD = 'email'
# ACCOUNT_USER_MODEL_USERNAME_FIELD = 'email'
ACCOUNT_USER_MODEL_USERNAME_FIELD = None

ACCOUNT_AUTHENTICATION_METHOD = 'email'
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_UNIQUE_EMAIL = True
ACCOUNT_EMAIL_VERIFICATION = None

ACCOUNT_ADAPTER = 'luxdjango.users.adapters.AccountAdapter'

GEOIP_PATH = os.path.join(BASE_DIR, 'geoip', 'GeoLite2-City.mmdb')

ROSETTA_STORAGE_CLASS = 'rosetta.storage.CacheRosettaStorage'
ROSETTA_ACCESS_CONTROL_FUNCTION = "luxdjango.settings.common.has_rosetta_access"


def has_rosetta_access(user):
    return user.is_authenticated() and user.is_staff

# https://support.google.com/a/answer/176600?hl=ru
# follow link below and give access to unverified applications
# https://www.google.com/settings/security/lesssecureapps
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = ''
EMAIL_HOST_PASSWORD = ''


ROSETTA_WSGI_AUTO_RELOAD = True

HAYSTACK_CONNECTIONS = {
    'default': {
        'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
        'URL': 'http://127.0.0.1:9200/',
        'INCLUDE_SPELLING': True,
        'INDEX_NAME': 'haystack',
        'EXCLUDED_INDEXES': [
            'realty.search_indexes.RealtyIndex',
            'vehicles.search_indexes.CarIndex'
        ]
    },
    'vehicles': {
        'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
        'URL': 'http://127.0.0.1:9200/',
        'INCLUDE_SPELLING': True,
        'INDEX_NAME': 'vehicles',
        'EXCLUDED_INDEXES': ['realty.search_indexes.RealtyIndex']
    },
    'realty': {
        'ENGINE': 'haystack.backends.elasticsearch_backend.ElasticsearchSearchEngine',
        'URL': 'http://127.0.0.1:9200/',
        'INCLUDE_SPELLING': True,
        'INDEX_NAME': 'realty',
        'EXCLUDED_INDEXES': ['vehicles.search_indexes.CarIndex']
    },
}
