# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals


from .common import *
try:
    from .local_settings import *
except ImportError:
    pass

SITE_ID = 2

DOMAIN = 'http://127.0.0.1:8000'

