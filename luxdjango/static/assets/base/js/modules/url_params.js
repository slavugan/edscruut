jQuery.extend({

    getQueryParameters: function (str) {
        str = str || document.location.search;
        return (!str && {}) || str.replace(/(^\?)/, '').split("&").map(function (n) {
                return n = n.split("="), this[n[0]] = decodeURIComponent(n[1]), this
            }.bind({}))[0];
    },

    encodeQueryParameters: function (parameters) {
        return '?' + encodeURI(JSON.stringify(parameters).replace(/,\"/g, '&\"').replace(/(\"|{|})/g, '').replace(/:/g, '='))
    }
});
