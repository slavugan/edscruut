function SearchPage(config) {

    var content = $('body');
    var semaphore = true;
    var disableScroll = true;
    var parameters = {'q': config.q_text};

    // put search text from post
    content.find(".fire-reload").val(config.q_text);

    // block enter key press
    $(".fire-reload").keypress(function (event) {
        if (event.keyCode === 10 || event.keyCode === 13)
            event.preventDefault();
    });

    var url_parameters = $.getQueryParameters();

    // if page reload
    if ($.isEmptyObject(url_parameters)) {
        ajaxGet(djangoContext.urls.ajaxSearch, parameters, function(content) {
            disableScroll = $('#search-list-result>div').length ? true : false;
        });
    } else {
        parameters = url_parameters
        ajaxGet(djangoContext.urls.ajaxSearch, parameters, function(content) {
            disableScroll = $('#search-list-result>div').length ? true : false;
        });
    }

    // Base request
    function sendRequest(k, v) {
        console.log('////send request////');
        if (k) {
            parameters[k] = v;
        }
        console.log(parameters);

        ajaxGet(djangoContext.urls.ajaxSearch, parameters, function(content) {
            if ('append-fragments' in content) {
                if (content['append-fragments']['#search-list-result'].length < 50) {
                    disableScroll = false;
                    if (parameters.page) {
                        if (Number(parameters.page) > 2) {
                            parameters.page--;
                        } else {
                            delete parameters.page;
                        }
                    }
                } else {
                    disableScroll = true;
                }
            } else if ($('#search-list-result>div').length) {
                disableScroll = true;
            } else {
                disableScroll = false;
            }
            delete parameters.scroll;
            history.pushState('filters', "current_state", $.encodeQueryParameters(parameters));
            semaphore = true;
        });

    }

    //main text filter
    content.find(".fire-reload").on("keyup", function (event) {
        parameters = {'models': config.model};
        sendRequest('q', this.value);
    });
    //Check box filters
    content.find(".fire-reload-filters").on("click", 'li div input', function (event) {
        delete parameters.page;
        if (parameters[this.name] == this.value) {
            delete parameters[this.name];
            sendRequest()
        } else {
            sendRequest(this.name, this.value);
        }
    });
    //Pagination
    //content.find(".fire-reload-pagination").on("click", '.pagination a', function(event) {
    //    sendRequest('page', $(this).attr('data_info'));
    //});

    //colors filter
    content.find("#colors-filters").on("click", '.color_faced_box', function (event) {
        delete parameters.page;
        if (parameters.color_index == $(this).attr('data_info')) {
            delete parameters.color_index;
            sendRequest()
        } else {
            sendRequest('color_index', $(this).attr('data_info'));
        }
    });

    content.find("#sort").on('change', '#sort', function () {
        delete parameters.page;
        if (this.value == 0) {
            delete parameters['sort'];
            sendRequest();
        } else {
            sendRequest('sort', this.value);
        }
    });

    //Double Range filters
    function rangeFilter(key) {
        // Initializing the slider
        var html5Slider = {};
        var step;

        if (key == 'year_index'){
            step = 1;
            var moneyFormat = wNumb({});
        }
        else {
            step=1000;
            var moneyFormat = wNumb({decimals: 3, thousand: '.', });
        }
        html5Slider[key] = document.getElementById(key + '-range_slider');

        noUiSlider.create(html5Slider[key], {
            start: parameters[key] ? parameters[key].split(",") : config.rangeFiltersList[key],
            step: step,
            connect: true,
            range: {
                'min': config.rangeFiltersList[key][0],
                'max': config.rangeFiltersList[key][1]
            },
            format: moneyFormat

        });

        var inputNumberL = {};
        var inputNumberR = {};

        inputNumberL[key] = document.getElementById(key + '-input-number-l');
        inputNumberR[key] = document.getElementById(key + '-input-number-r');

        html5Slider[key].noUiSlider.on('update', function (values, handle) {
            var value = values[handle];
            if (handle) {
                inputNumberL[key].value = value;
            } else {
                inputNumberR[key].value = value;
            }
        });

        html5Slider[key].noUiSlider.on('set', function (values, handle) {
            sendRequest(key, values[0] + ',' + values[1]);
        });

        inputNumberL[key].addEventListener('change', function () {
            sendRequest(key, html5Slider[key].noUiSlider.get()[0] + ',' + this.value);
            html5Slider[key].noUiSlider.set([null, this.value]);
        });

        inputNumberR[key].addEventListener('change', function () {
            sendRequest(key, this.value + ',' + html5Slider[key].noUiSlider.get()[1]);
            html5Slider[key].noUiSlider.set([this.value, null]);
        });
    }


    for (var item in config.rangeFiltersList) {
        rangeFilter(item);
    }

    //Radius range filters
    var snapSlider = document.getElementById('radius-rangefilter');

    noUiSlider.create(snapSlider, {
        start: parameters.distance ? parameters.distance : [1000],
        snap: true,
        range: {
            'min': 5,
            '10%': 20,
            '20%': 50,
            '30%': 100,
            '40%': 200,
            '50%': 300,
            '60%': 500,
            '70%': 800,
            'max': 1000
        }
    });

    var snapSliderValueElement = document.getElementById('radius-step-value');

    snapSlider.noUiSlider.on('update', function (values, handle) {
        if (values[0] != '1000.00') {
            var data = values[handle];
        } else {
            var data = gettext('WORLDWIDE');
        }
        snapSliderValueElement.innerHTML = data;
    });

    snapSlider.noUiSlider.on('change', function (values, handle) {
        delete parameters.page;
        sendRequest('distance', values[0]);
    });

    //filters  countries
    var country_filter = $(".chosen-select-country");
    country_filter.select2().val(parameters.countries ? parameters.countries.split(',') : []).trigger("change");
    $('.select2-search__field').css("min-width", "125px"); // by default select2 set width 100px it's not enough
    $(".chosen-wrapper").show().css("display", "inline-block");

    country_filter.on('change', function (evt) {
        delete parameters.page;
        sendRequest('countries', (country_filter.val() || []).join());
    });

    $('#locate-me').on('click', function () {
        getLocationWithAPI(getLocationCallback);
    });

    //scroll pagination
    var win = $(window);

    win.scroll(function () {
        var scrollTop80Percentage = parseInt(parseInt(win.scrollTop() + (win.scrollTop() / 100) * 30))

        if ($(document).height() - win.height() <= scrollTop80Percentage && semaphore == true) {
            parameters.page ? parameters.page++ : parameters.page = 2;
            parameters.scroll = true;
            semaphore = false;
            if (disableScroll) {
                sendRequest();
            }
        }
    });

    $('#left-sidebar-filters').on('change', '#property_type', function () {
        delete parameters.page;
        delete parameters['property_type_type']
        if (this.value == '0') {
            delete parameters['property_type'];
            sendRequest();
        } else {
            sendRequest('property_type', this.value);
        }
    });

    $('#left-sidebar-filters').on('change', '#city', function () {
        delete parameters.page;
        if (this.value == 0) {
            delete parameters['city'];
            sendRequest();
        } else {
            sendRequest('city', this.value);
        }
    });

    setLocation();

// start for search page
    var subscribe_dialog = $('#subscribe-dialog');

    subscribe_dialog.find('button.ok').attr('disabled', false);

    subscribe_dialog.find('button.ok').on('click', function () {
        $.ajax({
            method: 'post',
            url: djangoContext.urls.subscribe,
            data: {filters: JSON.stringify(parameters)},
            dataType: 'josn',
        }).done(function () {
            $('#subscribed-mess').modal('show');
        }).fail(function () {
            alert('Error');
        });
    });
// end for search page

    function setLocation() {
        getLocation(getLocationCallback);
    }

    function getLocationCallback(location) {
        var ltext = [location.city, ' (', location.country_code, ')'].join('');
        $('#your-location').text(ltext);
        $('.your-loc').show();
    }

    function getLocation(callback) {
        var location = getCookie('location');
        if (!location) {
            getLocationByIP(callback);
        } else {
            if (callback) callback(JSON.parse(decodeURI(location)));
        }
    }

    function getLocationByIP(callback) {
        $.ajax({
            method: 'get',
            url: '/get-location-by-ip/',
            dataType: 'json',
        }).done(function (resp) {
            setCookie('location', encodeURI(JSON.stringify(resp)));
            if (callback) callback(resp);
        }).fail(function () {
            getLocationWithAPI(callback);
        });
    }

    function getLocationWithAPI(callback) {
        if (navigator.geolocation) {
            $('#locate-me a').text('locating...');
            navigator.geolocation.getCurrentPosition(function (pos) {
                var current_lang = $('#current-lang').text();
                $.ajax({
                    method: 'get',
                    url: 'http://nominatim.openstreetmap.org/reverse',
                    data: {
                        format: 'json',
                        lat: pos.coords.latitude,
                        lon: pos.coords.longitude,
                        'accept-language': current_lang,
                    },
                    dataType: 'json',
                }).done(function (resp) {
                    var city = resp.address.city;
                    if (!city) city = resp.address.village;
                    var location = {
                        city: city,
                        country: resp.address.country,
                        country_code: resp.address.country_code.toUpperCase(),
                        latitude: pos.coords.latitude,
                        longitude: pos.coords.longitude,
                    };
                    setCookie('location', encodeURI(JSON.stringify(location)));
                    if (callback) callback(location);
                    $('#locate-me a').text('locate me');
                }).fail(function (resp) {
                    $('#locate-me a').text('locate me');
                    alert(gettext('Unable to locate'));
                });
            });
        } else {
            alert('You browser doesnt support geo location ')
        }
    }

}
