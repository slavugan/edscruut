# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('users', '0002_subscription'),
    ]

    operations = [
        migrations.AddField(
            model_name='subscription',
            name='last_notification',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
