from django.db import models
from django.contrib.auth.models import AbstractBaseUser
from luxdjango.users.managers import UserManager
from vehicles.models import Country


class User(AbstractBaseUser):
    email = models.EmailField(verbose_name='email address', max_length=255, unique=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)
    country = models.ForeignKey(Country, null=True)

    objects = UserManager()
    USERNAME_FIELD = 'email'

    def get_full_name(self):
        # The user is identified by their email address
        return self.email

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def __unicode__(self):
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin


class Subscription(models.Model):
    CATEGORY_CHOICES = ((1, 'vehicles'), (2, 'realty'))
    user = models.ForeignKey(User)
    filters = models.CharField(max_length=500)
    last_notification = models.DateTimeField(null=True, blank=True)
    category = models.PositiveIntegerField(choices=CATEGORY_CHOICES)