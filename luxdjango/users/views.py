# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals
import json, re

from django.conf import settings
from django.core.urlresolvers import reverse
from django.http import HttpResponse
from django.utils.translation import ugettext as _
from django.shortcuts import redirect
from django.views.generic import DetailView, ListView, RedirectView, UpdateView, View

#from django.contrib.auth.mixins import LoginRequiredMixin
from braces.views import LoginRequiredMixin
from .models import User, Subscription
from vehicles.models import Country
from realty.models import PropertyType, PropertyTypeType, City


class UserDetailView(LoginRequiredMixin, DetailView):
    model = User
    # These next two lines tell the view to index lookups by email
    slug_field = "email"
    slug_url_kwarg = "email"

    def get_context_data(self, **kwargs):
        context = super(UserDetailView, self).get_context_data(**kwargs)
        subscriptions = self.request.user.subscription_set.all()
        result = {}

        for s in subscriptions:
            raw_filters = json.loads(s.filters)
            filters = {}

            if 'property_type_type' in raw_filters:
                filters['Property type'] = PropertyTypeType.objects.filter(
                    id=raw_filters['property_type_type']
                ).first().name
            elif 'property_type' in raw_filters:
                filters['Property type'] = PropertyType.objects.filter(
                    id=raw_filters['property_type']
                ).first().name
            if 'city' in raw_filters:
                filters['City'] = City.objects.get(id=raw_filters['city'])
            if 'deal' in raw_filters:
                filters[_('Deal')] = _('Sale') if raw_filters['deal'] == 1 else _('Rent')

            for key in raw_filters:
                key2 = None
                if key.endswith('_index'):
                    key2 = key.replace('_index', '')
                elif key == 'q' and raw_filters[key]:
                    key2 = 'text'
                elif key == 'countries' and raw_filters[key]:
                    key2 = key
                    country_ids = [int(x) for x in raw_filters[key].split(',')]
                    country_names = Country.objects.filter(
                        id__in=country_ids
                    ).values_list('name', flat=True)
                    raw_filters[key] = ', '.join(country_names)
                elif key == 'distance':
                    # user location should be in user model to filter search by distance
                    continue
                elif key in ['price_index', 'year_index', 'mileage_index', 'year_index']:
                    key2 = key
                    raw_filters[key] = raw_filters[key].replace(',', ' - ')

                if key2:
                    key2 = key2.replace('_', ' ').capitalize()
                    filters[key2] = raw_filters[key]
                    if key2 not in ['Brand', 'Countries']:
                        filters[key2] = filters[key2].lower()



            if not filters:
                filters['Without filters'] = 'any updates'
            result[s.id] = filters

        context['subscriptions'] = result
        return context


class UserRedirectView(LoginRequiredMixin, RedirectView):
    permanent = False

    def get_redirect_url(self):

        return reverse("users:detail",
                       kwargs={"email": self.request.user.email})


class UserUpdateView(LoginRequiredMixin, UpdateView):
    fields = ['email', ]
    model = User

    # send the user back to their own page after a successful update
    def get_success_url(self):
        return reverse("users:detail",
                       kwargs={"email": self.request.user.email})

    def get_object(self):
        # Only get the User record for the user making the request
        return User.objects.get(email=self.request.user.email)


class UserListView(LoginRequiredMixin, ListView):
    model = User
    # These next two lines tell the view to index lookups by email
    slug_field = "email"
    slug_url_kwarg = "email"


class Subscribe(View):

    def post(self, request):
        category = re.match(r'^[^\/]+\/\/[^\/]+\/(\w+)', request.META['HTTP_REFERER']).groups()[0]
        if category == 'vehicles':
            category = 1
        elif category == 'realty':
            category = 2
        if request.user.is_authenticated():
            data = json.loads(request.POST['filters'])
            if 'page' in data:
                del data['page']
            if 'scroll' in data:
                del data['scroll']
            for key in ['year_index', 'mileage_index']:
                if key in data:
                    data[key] = data[key].replace('.00', '')
            Subscription.objects.get_or_create(filters=json.dumps(data),
                                               user=request.user,
                                               category=category)
            return HttpResponse(status=204)
        else:
            return HttpResponse('Not authenticated!', status=400)


class DeleteSubscriptions(LoginRequiredMixin, View):

    def post(self, request):
        ids = json.loads(request.POST['subscriptions'])
        request.user.subscription_set.filter(id__in=ids).delete()
        return HttpResponse(status=204)


def social_redirect(request):
    next_url = request.GET.get('url', ).replace('|', '?').replace('^', '&')
    if next_url:
        return redirect(to=next_url)
    return redirect(to=settings.LOGIN_REDIRECT_URL)