# -*- coding: utf-8 -*-
from allauth.utils import resolve_url
from django.conf import settings
from allauth.account.adapter import DefaultAccountAdapter
from allauth.socialaccount.adapter import DefaultSocialAccountAdapter


class AccountAdapter(DefaultAccountAdapter):
    def is_open_for_signup(self, request):
        return getattr(settings, 'ACCOUNT_ALLOW_REGISTRATION', True)

    # def get_login_redirect_url(self, request):
    #     url = settings.LOGIN_REDIRECT_URL
    #     r = request
    #     print r
    #     import pdb; pdb.set_trace()
    #     return resolve_url(url)



class SocialAccountAdapter(DefaultSocialAccountAdapter):
    def is_open_for_signup(self, request, sociallogin):
        return getattr(settings, 'ACCOUNT_ALLOW_REGISTRATION', True)



