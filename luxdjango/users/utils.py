# -*- coding: utf-8 -*-
import json
from django.utils import timezone
from django.core.mail import EmailMultiAlternatives
from django.template import Context
from django.template.loader import get_template
from django.conf import settings

from .models import Subscription
from vehicles.views import ElasticSearchAjax as vehicle_search
from realty.views import ElasticSearchAjax as realty_search

categories = ((vehicle_search, 1, 'email/car_updates.html'),
              (realty_search, 2, 'email/realty_updates.html'))

def send_notification():
    for ElasticSearchAjax, category, template in categories:
        subscriptions = Subscription.objects.filter(category=category)
        el_search = ElasticSearchAjax()
        el_search.request = lambda: None
        template = get_template(template)

        for subscr in subscriptions:
            el_search.request.GET = json.loads(subscr.filters)
            q = el_search.queryset()
            if subscr.last_notification:
                q = q.filter(date_index__gt=subscr.last_notification)
            total = q.count()

            if total:
                print('send mes')
                object_list = q.order_by('-id')[:3]
                context = Context({
                    'object_list': object_list,
                    'total': total,
                    'domain': settings.DOMAIN
                })
                html_content = template.render(context)
                emess = EmailMultiAlternatives(
                    subject='Last updates',
                    body=html_content,
                    to=[subscr.user.email],
                )
                emess.content_subtype = 'html'
                emess.send()

                subscr.last_notification = timezone.now()
                subscr.save()
