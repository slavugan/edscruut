# -*- coding: utf-8 -*-
import json
from django.http import HttpResponse
from django.views.generic import View

from geoip.utils import get_location_by_ip


class GetLocationByIPView(View):

    def get(self, request):
        location = get_location_by_ip(request)
        if location:
            resp = {
                'city': location.city.name,
                'country': location.country.name,
                'country_code': location.country.iso_code,
                'latitude': location.location.latitude,
                'longitude': location.location.longitude,
            }
            return HttpResponse(json.dumps(resp))
        else:
            return HttpResponse(json.dumps({'error': 'Unable to locate'}))