from geoip2 import database as geodb
from luxdjango.settings.common import GEOIP_PATH
from django.utils.translation import get_language

def get_location_by_ip(request):
    ip = request.META['REMOTE_ADDR']
    geo_reader = geodb.Reader(GEOIP_PATH, locales=[get_language()])
    try:
        location = geo_reader.city(ip)
    except:
        location = None
    return location
