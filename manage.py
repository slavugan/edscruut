#!/usr/bin/env python
import os
import sys
import django

if __name__ == "__main__":
    path = os.getcwd()
    if 'home/ansun' in path or 'home/slav' in path or 'dmitry' in path:
        os.environ.setdefault("DJANGO_SETTINGS_MODULE", "luxdjango.settings.dev")
    else:
        os.environ.setdefault("DJANGO_SETTINGS_MODULE", "luxdjango.settings.production")
    from django.core.management import execute_from_command_line

    execute_from_command_line(sys.argv)
