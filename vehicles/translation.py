# -*- coding: utf-8 -*-
from modeltranslation.translator import register, TranslationOptions

from .models import Country, BaseColor, Color


@register(Country)
class CountryTranslationOptions(TranslationOptions):
    fields = ('name',)


@register(BaseColor)
class BaseColorTranslationOptions(TranslationOptions):
    fields = ('color',)


@register(Color)
class ColorTranslationOptions(TranslationOptions):
    fields = ('color',)