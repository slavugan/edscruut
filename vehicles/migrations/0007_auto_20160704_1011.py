# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0006_auto_20160703_1912'),
    ]

    operations = [
        migrations.RenameField(
            model_name='seller',
            old_name='seller',
            new_name='name',
        ),
        migrations.RenameField(
            model_name='seller',
            old_name='contact',
            new_name='phones',
        ),
        migrations.AddField(
            model_name='seller',
            name='seller_type',
            field=models.PositiveIntegerField(default=1, verbose_name='Seller type', choices=[(1, 'Private seller'), (2, 'Professional seller')]),
        ),
        migrations.AlterField(
            model_name='car',
            name='color',
            field=models.CharField(blank=True, max_length=50, null=True, verbose_name='Color', choices=[('beige', 'Beige'), ('white', 'White'), ('blue', 'Blue'), ('grey', 'Grey'), ('yellow', 'Yellow'), ('black', 'Black'), ('orange', 'Orange'), ('pink', 'Pink'), ('red', 'Red'), ('green', 'Green'), ('purple', 'Purple')]),
        ),
    ]
