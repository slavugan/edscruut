# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import vehicles.models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='car',
            name='color',
            field=models.CharField( max_length=50, verbose_name='Color', choices=[('beige', 'Beige'), ('white', 'White'), ('blue', 'Blue'), ('grey', 'Grey'), ('yellow', 'Yellow'), ('black', 'Black'), ('orange', 'Orange'), ('pink', 'Pink'), ('red', 'Red'), ('green', 'Green'), ('purple', 'Purple')]),
        ),
        migrations.AlterField(
            model_name='vehicle',
            name='of_type',
            field=models.CharField(default=1, max_length=50, verbose_name='Vehicle Type', choices=[(1, 'Car'), (2, 'MotorCycle'), (3, 'Utility')]),
        ),
    ]
