# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0029_auto_20160809_1513'),
    ]

    operations = [
        migrations.AlterField(
            model_name='car',
            name='gear_box',
            field=models.CharField(default='', max_length=50, verbose_name='Gear Box', blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='car',
            name='model_type',
            field=models.CharField(max_length=100, null=True, verbose_name='Model type', blank=True),
        ),
        migrations.AlterField(
            model_name='seller',
            name='seller_type',
            field=models.PositiveIntegerField(default=1, verbose_name='Seller type', choices=[(1, 'Private seller'), (2, 'Professional  seller')]),
        ),
    ]
