# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0023_color_base_color'),
    ]

    operations = [
        migrations.AddField(
            model_name='basecolor',
            name='color_en',
            field=models.CharField(max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='basecolor',
            name='color_fr',
            field=models.CharField(max_length=50, null=True),
        ),
    ]
