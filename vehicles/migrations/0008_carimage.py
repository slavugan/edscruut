# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import vehicles.models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0007_auto_20160704_1011'),
    ]

    operations = [
        migrations.CreateModel(
            name='CarImage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('image', models.ImageField(upload_to=vehicles.models.img_upload_to)),
                ('car', models.ForeignKey(to='vehicles.Car')),
            ],
        ),
    ]
