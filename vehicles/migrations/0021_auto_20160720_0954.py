# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0020_auto_20160719_1301'),
    ]

    operations = [
        migrations.CreateModel(
            name='BaseColor',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('color', models.CharField(max_length=50)),
            ],
        ),
        # migrations.AlterField(
        #     model_name='color',
        #     name='base_color',
        #     field=models.ForeignKey(related_name='uscolor', blank=True, to='vehicles.BaseColor', null=True),
        # ),
    ]
