# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0008_carimage'),
    ]

    operations = [
        migrations.AddField(
            model_name='car',
            name='price_history',
            field=models.TextField(default='[]'),
        ),
    ]
