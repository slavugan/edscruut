# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0013_auto_20160708_0920'),
    ]

    operations = [
        migrations.AddField(
            model_name='car',
            name='color',
            field=models.ForeignKey(to='vehicles.Color', null=True),
        ),
        migrations.AddField(
            model_name='car',
            name='color_inside',
            field=models.ForeignKey(related_name='car_inside', to='vehicles.Color', null=True),
        ),
    ]
