# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    operations = [
        migrations.CreateModel(
            name='Brand',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('brand_code', models.CharField(max_length=30, null=True, verbose_name='Brand Code', blank=True)),
                ('brand_name', models.CharField(max_length=50, null=True, verbose_name='Brand Name', blank=True)),
            ],
        ),
        migrations.CreateModel(
            name='Car',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('finition', models.CharField(max_length=50, null=True, verbose_name='Finition')),
                ('model_type', models.CharField(max_length=50, null=True, verbose_name='Type')),
                ('model_month', models.IntegerField(default=5, verbose_name='Model Month', choices=[(1, 1), (2, 2), (3, 3), (4, 4), (5, 5), (6, 6), (7, 7), (8, 8), (9, 9), (10, 10), (11, 11), (12, 12)])),
                ('model_year', models.IntegerField(default=2016, verbose_name='Model year', choices=[(1980, 1980), (1981, 1981), (1982, 1982), (1983, 1983), (1984, 1984), (1985, 1985), (1986, 1986), (1987, 1987), (1988, 1988), (1989, 1989), (1990, 1990), (1991, 1991), (1992, 1992), (1993, 1993), (1994, 1994), (1995, 1995), (1996, 1996), (1997, 1997), (1998, 1998), (1999, 1999), (2000, 2000), (2001, 2001), (2002, 2002), (2003, 2003), (2004, 2004), (2005, 2005), (2006, 2006), (2007, 2007), (2008, 2008), (2009, 2009), (2010, 2010), (2011, 2011), (2012, 2012), (2013, 2013), (2014, 2014), (2015, 2015), (2016, 2016)])),
                ('km', models.IntegerField(null=True, verbose_name='Kilo Meters')),
                ('price', models.IntegerField(null=True, verbose_name='Price')),
                ('energy', models.CharField(max_length=50, null=True, verbose_name='Energy')),
                ('gear_box', models.CharField(max_length=50, null=True, verbose_name='Gear Box')),
                ('cylinder', models.CharField(max_length=50, null=True, verbose_name='Cylinder')),
                ('power', models.CharField(max_length=50, null=True, verbose_name='Power')),
                ('color', models.CharField(max_length=50, null=True, verbose_name='Color')),
                ('color_inside', models.CharField(max_length=50, null=True, verbose_name='Collor Inside')),
                ('doors', models.IntegerField(null=True, verbose_name='Doors')),
                ('url', models.URLField(unique=True, verbose_name='URL')),
                ('latitude', models.FloatField(null=True, blank=True)),
                ('longitude', models.FloatField(null=True, blank=True)),
                ('brand', models.ForeignKey(to='vehicles.Brand', null=True)),
            ],
        ),
        migrations.CreateModel(
            name='Manufacturer',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('manufacturer_code', models.CharField(max_length=50, null=True, verbose_name='Manufacturer Code')),
                ('manufacturer_name', models.CharField(max_length=50, null=True, verbose_name='Manufacturer Name')),
                ('manufacturer_details', models.TextField(null=True, verbose_name='Manufacturer Detials')),
            ],
        ),
        migrations.CreateModel(
            name='Model',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('model_code', models.CharField(max_length=50, verbose_name='Model Code')),
                ('model_name', models.CharField(max_length=50, verbose_name='Model Name')),
                ('brand', models.ForeignKey(to='vehicles.Brand')),
            ],
        ),
        migrations.CreateModel(
            name='Seller',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('seller', models.CharField(max_length=50, null=True, verbose_name='Seller')),
                ('contact', models.CharField(max_length=50, null=True, verbose_name='Phone')),
                ('location', models.CharField(max_length=50, null=True, verbose_name='Address')),
            ],
        ),
        migrations.CreateModel(
            name='Vehicle',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('of_type', models.CharField(max_length=50, null=True, verbose_name='Vehicle Type', choices=[(1, 'Car'), (2, 'MotorCycle'), (3, 'Utility')])),
            ],
        ),
        migrations.AddField(
            model_name='car',
            name='model',
            field=models.ForeignKey(to='vehicles.Model', null=True),
        ),
        migrations.AddField(
            model_name='car',
            name='seller',
            field=models.ForeignKey(to='vehicles.Seller', null=True),
        ),
        migrations.AddField(
            model_name='car',
            name='vehicle_type',
            field=models.ForeignKey(default=1, to='vehicles.Vehicle'),
        ),
    ]
