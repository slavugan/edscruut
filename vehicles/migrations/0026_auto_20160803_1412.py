# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0025_auto_20160720_1142'),
    ]

    operations = [
        migrations.AlterField(
            model_name='car',
            name='country',
            field=models.ForeignKey(blank=True, to='vehicles.Country', null=True),
        ),
        migrations.AlterField(
            model_name='car',
            name='doors',
            field=models.PositiveIntegerField(null=True, verbose_name='Doors'),
        ),
        migrations.AlterField(
            model_name='car',
            name='km',
            field=models.PositiveIntegerField(null=True, verbose_name='Kilo Meters'),
        ),
        migrations.AlterField(
            model_name='car',
            name='price',
            field=models.PositiveIntegerField(null=True, verbose_name='Price'),
        ),
        migrations.AlterField(
            model_name='car',
            name='url',
            field=models.URLField(unique=True, null=True, verbose_name='URL'),
        ),
    ]
