# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0024_auto_20160720_1003'),
    ]

    operations = [
        migrations.AddField(
            model_name='color',
            name='color_en',
            field=models.CharField(max_length=50, null=True),
        ),
        migrations.AddField(
            model_name='color',
            name='color_fr',
            field=models.CharField(max_length=50, null=True),
        ),
    ]
