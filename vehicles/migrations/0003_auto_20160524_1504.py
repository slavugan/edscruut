# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0002_auto_20160523_1907'),
    ]

    operations = [
        migrations.CreateModel(
            name='Country',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, null=True, verbose_name='Country')),
            ],
        ),
        migrations.AddField(
            model_name='car',
            name='country',
            field=models.ForeignKey(to='vehicles.Country', null=True),
        ),
    ]
