# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0016_car_price_history'),
    ]

    operations = [
        migrations.AddField(
            model_name='car',
            name='weight',
            field=models.PositiveIntegerField(null=True, blank=True),
        ),
    ]
