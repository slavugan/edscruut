# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0012_auto_20160707_1545'),
    ]

    operations = [
        migrations.CreateModel(
            name='Color',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('color', models.CharField(max_length=20)),
                ('base_color', models.CharField(max_length=10, null=True, choices=[('beige', 'Beige'), ('white', 'White'), ('blue', 'Blue'), ('grey', 'Grey'), ('yellow', 'Yellow'), ('black', 'Black'), ('orange', 'Orange'), ('pink', 'Pink'), ('red', 'Red'), ('green', 'Green'), ('purple', 'Purple')])),
            ],
        ),
        migrations.RemoveField(
            model_name='car',
            name='color',
        ),
        migrations.RemoveField(
            model_name='car',
            name='color_inside',
        ),
    ]
