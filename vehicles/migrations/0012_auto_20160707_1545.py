# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0011_merge'),
    ]

    operations = [
        migrations.AddField(
            model_name='car',
            name='description',
            field=models.TextField(blank=True),
        ),
        migrations.AddField(
            model_name='car',
            name='options',
            field=models.TextField(blank=True),
        ),
    ]
