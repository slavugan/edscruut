# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0028_auto_20160809_1026'),
    ]

    operations = [
        migrations.AlterField(
            model_name='car',
            name='energy',
            field=models.CharField(default='', max_length=50, verbose_name='Fuel', blank=True),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='car',
            name='model_type',
            field=models.CharField(max_length=50, null=True, verbose_name='Model type', blank=True),
        ),
    ]
