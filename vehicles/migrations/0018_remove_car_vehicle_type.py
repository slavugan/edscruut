# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vehicles', '0017_car_weight'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='car',
            name='vehicle_type',
        ),
    ]
