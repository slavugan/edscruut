# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

import datetime, json, random, re
import hashlib

from django.contrib.gis.geos import Point
from django.contrib.postgres.fields import ArrayField
from django.core import validators
from django.core.exceptions import ValidationError
from django.db import models
from django.utils import timezone
from django.utils.translation import ugettext as _

YEAR_CHOICES = [(r, r) for r in range(1886, (datetime.datetime.now().year + 1))]
MONTH_CHOICES = [(r, r) for r in range(1, 13)]
YEARMONTH_INPUT_FORMATS = (
    '%m-%Y', '%m/%Y', '%m/%y',  # '2006-10', '10/2006', '10/06'
)


# sssssssssssss >
# class YearMonthField(models.CharField):
#     default_error_messages = {
#         'invalid': _('Enter a valid year and month.'),
#     }

#     def __init__(self, input_formats=None, *args, **kwargs):
#         super(YearMonthField, self).__init__(*args, **kwargs)
#         self.input_formats = input_formats

#     def clean(self, value):
#         if value in validators.EMPTY_VALUES:
#             return None
#         if isinstance(value, datetime.datetime):
#             return format(value, '%Y-%m')
#         if isinstance(value, datetime.date):
#             return format(value, '%Y-%m')
#         for fmt in self.input_formats or YEARMONTH_INPUT_FORMATS:
#             try:
#                 date = datetime.date(*datetime.time.strptime(value, fmt)[:3])
#                 return format(date, '%Y-%m')
#             except ValueError:
#                 continue
#         raise ValidationError(self.error_messages['invalid'])
# sssssssssss <


class Manufacturer(models.Model):
    manufacturer_code = models.CharField(max_length=50, null=True, verbose_name=_('Manufacturer Code'))
    manufacturer_name = models.CharField(max_length=50, null=True, verbose_name=_('Manufacturer Name'))
    manufacturer_details = models.TextField(null=True, verbose_name=_('Manufacturer Detials'))

    def __unicode__(self):
        return "%s %s %s" % (self.manufacturer_code, self.manufacturer_name, self.manufacturer_details)


class Brand(models.Model):
    # Vehicle = models.ForeignKey(Vehicle)
    brand_code = models.CharField(max_length=30, blank=True, null=True, verbose_name=_('Brand Code'))
    brand_name = models.CharField(max_length=50, blank=True, null=True, verbose_name=_('Brand Name'))

    def __unicode__(self):
        return self.brand_name


class Model(models.Model):
    brand = models.ForeignKey(Brand)
    model_code = models.CharField(max_length=50, verbose_name=_('Model Code'))
    model_name = models.CharField(max_length=50, verbose_name=_('Model Name'))

    def __unicode__(self):
        return unicode(self.model_name)


SELLER_CHOICES = ((1, _('Private seller')),
                  (2, _('Professional  seller')))


class Seller(models.Model):
    name = models.CharField(_('Seller'), max_length=100, null=True)
    phones = models.CharField(_('Phone'), max_length=100, null=True)
    location = models.CharField(_('Address'), max_length=100, null=True)
    seller_type = models.PositiveIntegerField(_('Seller type'), choices=SELLER_CHOICES, default=1)

    def __unicode__(self):
        return unicode(self.seller)


class Country(models.Model):
    name = models.CharField(_('Country'), max_length=50)

    def __unicode__(self):
        return self.name


class BaseColor(models.Model):
    color = models.CharField(max_length=50)


class Color(models.Model):
    color = models.CharField(max_length=50)
    base_color = models.ForeignKey(BaseColor, null=True, blank=True, related_name='uscolor')

    def __unicode__(self):
        return self.color


class Car(models.Model):
    VEHICLE_CHOICES = (
        (1, _('Car')),
        (2, _('MotorCycle')),
        (3, _('Utility'))
    )

    country = models.ForeignKey(Country, null=True, blank=True)
    vehicle_type = models.PositiveIntegerField(choices=VEHICLE_CHOICES, default=1)
    brand = models.ForeignKey(Brand, null=True)
    model = models.ForeignKey(Model, null=True)
    finition = models.CharField(_('Finition'), max_length=50, null=True, blank=True)
    model_type = models.CharField(_('Model type'), max_length=100, null=True, blank=True)
    model_month = models.IntegerField(_('Model Month'), choices=MONTH_CHOICES, null=True, blank=True)
    model_year = models.IntegerField(_('Model year'), choices=YEAR_CHOICES, null=True, blank=True)
    seller = models.ForeignKey(Seller, null=True)
    km = models.PositiveIntegerField(_('Mileage'), null=True, blank=True)
    price = models.PositiveIntegerField(_('Price'), null=True, blank=True)
    energy = models.CharField(_('Fuel'), max_length=50, blank=True)
    gear_box = models.CharField(_('Gear Box'), max_length=50, blank=True)
    cylinder = models.CharField(_('Cylinder volume'), max_length=50, null=True, blank=True)
    power = models.CharField(_('Power'), max_length=50, null=True, blank=True)
    color = models.ForeignKey(Color, null=True, blank=True)
    color_inside = models.ForeignKey(Color, null=True, related_name='car_inside', blank=True)
    doors = models.PositiveIntegerField(_('Doors'), null=True, blank=True)
    url = models.URLField("URL", null=True, blank=True)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    creation_date = models.DateTimeField(default=timezone.now)
    places = models.PositiveIntegerField(blank=True, null=True)
    weight = models.PositiveIntegerField(blank=True, null=True)
    price_history = ArrayField(models.IntegerField(), blank=True, null=True)
    options = models.TextField(blank=True)
    description = models.TextField(blank=True)

    def get_options(self):
        return json.loads(self.options)

    def phones_as_list(self):
        return self.seller.phones.split(',')

    def __unicode__(self):
        return unicode(self.brand.brand_name)

    def get_location(self):
        if self.longitude and self.latitude:
            return Point(self.longitude, self.latitude)
        else:
            return None

    def update_price(self, new_price):
        if self.price != new_price:
            if self.price_history:
                index = len(self.price_history) - 1
                if self.price and self.price != self.price_history[index]:
                    self.price_history.append(self.price)
                elif new_price == self.price_history[index]:
                    del self.price_history[index]
            elif self.price:
                self.price_history = [self.price]
            self.price = new_price
            self.save()

    @property
    def domain(self):
        if self.url:
            domain = re.search(r'\/\/([^\/]+)', self.url)
            if domain:
                return domain.groups()[0]
        return ''


def get_hash_name(brand, model, filename):
    ext = filename.split('.')[-1]
    hash_object = hashlib.md5(
        str(brand).decode('utf-8').encode('utf-8') + str(model).encode() + str(datetime.datetime.now()).encode())
    return '{}.{}'.format(hash_object.hexdigest(), ext)


def img_upload_to(instance, filename):
    return 'car-us/%s/%s/%s' % (
        datetime.datetime.now().year, instance.car_id, get_hash_name(instance.car.brand, instance.car.model, filename))


class CarImage(models.Model):
    car = models.ForeignKey(Car)
    image = models.ImageField(upload_to=img_upload_to, blank=False)
