from django.core.management import BaseCommand
from vehicles.models import *


class Command(BaseCommand):
    def handle(self, *args, **options):
        for i in Car.objects.all():
            i.country = Country.objects.get(id=random.randint(1, Country.objects.count()))
            print i.country
            i.save()