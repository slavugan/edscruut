from haystack import indexes
from .models import Car


class CarIndex(indexes.SearchIndex, indexes.Indexable):
    text = indexes.CharField(document=True, use_template=True)
    gear_box_index = indexes.CharField(model_attr='gear_box', faceted=True, default=None)
    color_index = indexes.CharField(model_attr='color__base_color__color', faceted=True, default=None)
    brand_index = indexes.CharField(model_attr='brand', faceted=True, default=None)
    model_index = indexes.CharField(model_attr='model', default=None)
    energy_index = indexes.CharField(model_attr='energy', faceted=True, default=None)
    location_index = indexes.LocationField(model_attr='get_location', default=None)
    seller_index = indexes.CharField(model_attr='seller__seller_type', default=None, faceted=True)
    year_index = indexes.IntegerField(model_attr='model_year', default=None)
    mileage_index = indexes.IntegerField(model_attr='km', default=None)
    price_index = indexes.IntegerField(model_attr='price', default=None)
    country_index = indexes.IntegerField(model_attr='country__id', faceted=True, default=None)
    date_index = indexes.DateTimeField(model_attr='creation_date')

    # date of insertion

    def get_model(self):
        return Car

    def index_queryset(self, using=None):
        return self.get_model().objects.all()
