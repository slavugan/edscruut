# -*- coding: utf-8 -*-

import copy, urllib
import simplejson as json

from django.core.urlresolvers import reverse
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from django.views.generic import TemplateView, DetailView, FormView
from django_ajax.mixin import AJAXMixin
from haystack.inputs import Raw
from haystack.query import SearchQuerySet
from haystack.utils.geo import Point, D

from geoip.utils import get_location_by_ip
from vehicles.form import VehiclesForm, CarFormSet
from vehicles.models import Country, Car, CarImage, Brand, Model, Color, SELLER_CHOICES



class MainCarView(TemplateView):
    template_name = 'search/vehicle_search.html'

    def post(self, request, *args, **kwargs):
        context = self.get_context_data()
        context['post_q'] = request.POST.get('q')
        return super(TemplateView, self).render_to_response(context)

    def get_context_data(self, **kwargs):
        context = super(MainCarView, self).get_context_data()
        context['location'] = get_location_by_ip(self.request)
        context['list_of_country'] = Country.objects.all()
        return context


class DetailCarView(DetailView):
    model = Car


AJAX_TEMPLATE = {
    'fragments': {
        '#id1': 'replace element with this content1'
    },
    'inner-fragments': {
        '#replace-inner-content': 'replace inner content'
    },
    'append-fragments': {
        '.class1': 'append this content'
    },
    'prepend-fragments': {
        '.class2': 'prepend this content'
    }
}


def autocomplete(request):
    sqs = SearchQuerySet().autocomplete(brand_auto=request.GET.get('q', ''))[:5]
    suggestions = [result.km for result in sqs]
    # Make sure you return a JSON object, not a bare list.
    # Otherwise, you could be vulnerable to an XSS attack.
    the_data = json.dumps({
        'results': suggestions
    })
    return HttpResponse(the_data, content_type='application/json')


class ElasticSearchAjax(AJAXMixin, TemplateView):
    FACET = [
        'brand_index',
        'gear_box_index',
        'color_index',
        'energy_index',
        'seller_index',
        'country_index',
    ]

    RANGE_FILTER = [
        'year_index',
        'mileage_index',
        'price_index',
    ]

    def __init__(self):
        self.q = SearchQuerySet(using='vehicles')

    def queryset(self):
        q = self.q
        r = self.request.GET

        if r.get('q'):
            q = q.filter(text=r.get('q'))
        for p in r:
            if p in self.FACET:
                if (p == "energy_index" or p == "gear_box_index") and r.get(p) == "not_provided":
                    params = {p: Raw('*')}
                    q = q.exclude(**params)
                else:
                    if (p == "seller_index"):
                        if r.get(p) == "Private seller":
                            params = {p: 1}
                        if r.get(p) == "Professional  seller":
                            params = {p: 2}
                    else:
                        params = {p: r.get(p)}
                    q = q.filter(**params)

        for p in r:
            if p in self.RANGE_FILTER:
                y = r.get(p).split(',')
                params = {p + '__range': [int(y[0].replace('.', '')), int(y[1].replace('.', ''))]}
                print params
                q = q.filter(**params)

        country = r.get('countries')
        if country:
            q = q.filter(country_index__in=country.split(','))

        distance = r.get('distance')
        if distance and distance != '1000.00':
            distance = D(km=int(r.get('distance').replace('.00', '')))

            if 'location' in self.request.COOKIES:
                location = json.loads(urllib.unquote(self.request.COOKIES['location']))
                latitude = location['latitude']
                longitude = location['longitude']
            else:
                location = get_location_by_ip(self.request)
                latitude = location.location.latitude
                longitude = location.location.longitude
            if location:
                user_point = Point(float(longitude), float(latitude))
                q = q.dwithin('location_index', user_point, distance)

        sort = r.get('sort')
        if sort:
            q = q.order_by(sort)

        return q

    def faceted(self, search_result):
        for f in self.FACET:
            search_result = search_result.facet(f)
        return search_result.facet_counts()

    def check_boxes_filters(self, facets):
        check_boxes_filters = copy.deepcopy(facets)
        del check_boxes_filters["color_index"]
        del check_boxes_filters["country_index"]
        return check_boxes_filters

    def get(self, request, *args, **kwargs):
        search_result = self.queryset()

        facets = self.faceted(search_result)['fields']
        seller_list = facets['seller_index']
        for counter, seller_tuple in enumerate(sorted(seller_list)):
            temp = list(seller_tuple)
            if seller_tuple[0] == str(SELLER_CHOICES[counter][0]):
                temp[0] = SELLER_CHOICES[counter][1]
            else:
                temp[0] = SELLER_CHOICES[(counter+1)][1]
            seller_list[counter] = tuple(temp)

        search_list_result = lambda: render(
            request,
            'search/car_item_preview.html',
            {'query': True, 'object_list': search_result }
        )

        left_sidebar_filters = lambda: render(
            request,
            'search/left_sidebar_filters.html',
            {'facets': self.check_boxes_filters(facets), 'conditions': request.GET}
        )

        color_filters = lambda: render(
            request,
            'search/colors_filter.html',
            {'colors': facets['color_index']}
        )

        if not request.GET.get('scroll'):
            AJAX_TEMPLATE = {'inner-fragments': {
                '#search-list-result': search_list_result(),
                '#left-sidebar-filters': left_sidebar_filters(),
                '#colors-filters': color_filters(),
            }}
        else:
            AJAX_TEMPLATE = {'append-fragments': {'#search-list-result': search_list_result(), }}

        return AJAX_TEMPLATE


class AddCarView(FormView):
    template_name = 'vehicles/car_form.html'
    form_class = VehiclesForm
    second_form_class = CarFormSet
    success_url = '/'

    def get(self, request):
        car_form = VehiclesForm()
        formset = CarFormSet()
        return render(request, self.template_name, {'car_image_form': formset, 'car_form': car_form})

    def post(self, request):
        car_form = VehiclesForm(data=request.POST)
        formset = CarFormSet(data=request.POST, files=request.FILES, queryset=CarImage.objects.none())
        if car_form.is_valid() and formset.is_valid():
            return self.form_valid(car_form, formset, request)
        return render(request, self.template_name, {'car_form': car_form, 'car_image_form': formset})

    def form_valid(self, car_form, formset, request):
        brand_name = car_form['brand'].value()
        model_name = car_form['model'].value()
        color_name = car_form['color'].value()
        color_inside_name = car_form['color_inside'].value()
        brand = Brand.objects.get_or_create(brand_name=brand_name)
        model = Model.objects.get_or_create(brand=brand[0], model_name=model_name)
        car = car_form.save(commit=False)
        if color_name:
            color = Color.objects.get_or_create(color=color_name)
            car.color = color[0]
        if color_inside_name:
            color_inside = Color.objects.get_or_create(color=color_inside_name)
            car.color_inside = color_inside[0]
        car.brand = brand[0]
        car.model = model[0]
        car.save()
        for form in formset:
            data = form.cleaned_data
            image = data.get('image')
            if image > 0:
                photo = CarImage(car=Car.objects.get(pk=car.id), image=image)
                photo.save()
        return HttpResponseRedirect(reverse('vehicles:car-details', kwargs={'pk': car.id}))


def get_brands(request):
    if request.is_ajax():
        q = request.GET.get('term', '')
        brands = Brand.objects.filter(brand_name__istartswith=q)[:20]
        results = []
        for brand in brands:
            brand_json = {}
            brand_json['brand_name'] = brand.brand_name
            results.append(brand_json['brand_name'])
        data = json.dumps(results)
        return HttpResponse(data)
    else:
        return HttpResponse(status=403)


def get_colors(request):
    if request.is_ajax():
        q = request.GET.get('term', '')
        colors = Color.objects.filter(color__istartswith=q)[:20]
        results = []
        for color in colors:
            color_json = {}
            color_json['color'] = color.color
            results.append(color_json['color'])
        data = json.dumps(results)
        return HttpResponse(data)
    else:
        return HttpResponse(status=403)


def get_models(request):
    if request.is_ajax():
        q = request.GET.get('term', '')
        brand = request.GET.get('id_brand', '')
        models = Model.objects.filter(model_name__istartswith=q)[:20]
        results = []
        for model in models:
            if model.brand.brand_name == brand or brand == "":
                model_json = {}
                model_json['model_name'] = model.model_name
                results.append(model_json['model_name'])
        data = json.dumps(results)
        return HttpResponse(data)
    else:
        return HttpResponse(status=403)


class HomeView(TemplateView):
    template_name = 'pages/home.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)
        context['vehicles_count'] = SearchQuerySet(using='vehicles').count()
        context['realty_count'] = SearchQuerySet(using='realty').count()
        context['total_count'] = context['realty_count'] + context['vehicles_count']
        return context