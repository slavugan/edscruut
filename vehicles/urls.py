# -*- coding: utf-8 -*-
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^add/', views.AddCarView.as_view(), name="add-vehicle"),
    url(r'^search/', views.MainCarView.as_view(), name="search"),
    url(r'^details/(?P<pk>[0-9]+)/$', views.DetailCarView.as_view(), name='car-details'),
    url(r'^search-ajax/', views.ElasticSearchAjax.as_view(), name='search-ajax'),
    url(r'^search/autocomplete/', views.autocomplete, name='autocomplete'),
    url(r'^postadd/$', views.AddCarView.as_view(), name="AddCar"),
    url(r'^get_models/', views.get_models, name='get_models'),
    url(r'^get_colors/', views.get_colors, name='get_colors'),
    url(r'^get_brands/', views.get_brands, name='get_brands'),
]
