from django import forms
from django.forms import TextInput, Select, Textarea, NumberInput, FileInput
from .models import Car, CarImage
from django.forms.models import inlineformset_factory


class VehiclesForm(forms.ModelForm):
    color = forms.CharField(required=False,
                            widget=forms.TextInput(
                                attrs={'class': 'form-control  c-square c-theme', 'title': 'Color'}))
    color_inside = forms.CharField(required=False,
                                   widget=forms.TextInput(
                                       attrs={'class': 'form-control  c-square c-theme', 'title': 'Color inside'}))
    brand = forms.CharField(required=True,
                            widget=forms.TextInput(
                                attrs={'class': 'form-control  c-square c-theme', 'title': 'Brand'}))
    model = forms.CharField(required=True,
                            widget=forms.TextInput(
                                attrs={'class': 'form-control  c-square c-theme', 'title': 'Model'}))

    class Meta:
        model = Car
        exclude = ['seller', 'url', 'price_history', 'creation_date', 'latitude', 'longitude', 'weight', 'brand',
                   'model', 'color', 'color_inside', 'vehicle_type']
        widgets = {
            'country': Select(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Country', 'placeholder': 'Country'}),
            'finition': TextInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Finition'}),
            'model_type': TextInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Model Type'}),
            'model_month': Select(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Model Month',
                       'placeholder': 'Model Month'}),
            'model_year': Select(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Model Year'}),
            'km': NumberInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Kilo Meters'}),
            'price': NumberInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Price'}),
            'energy': TextInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Energy'}),
            'gear_box': TextInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Gear box'}),
            'cylinder': TextInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Cylinder'}),
            'power': TextInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Power'}),
            'places': NumberInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Places'}),
            'doors': NumberInput(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Doors'}),
            'options': Textarea(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Options'}),
            'description': Textarea(
                attrs={'class': 'form-control  c-square c-theme', 'title': 'Description'}),
        }


CarFormSet = inlineformset_factory(Car, CarImage, fields=['image'], extra=30,
                                   widgets={'image': FileInput(attrs={'class': 'form-control  c-square c-theme',
                                                                      'style': 'Display: none;',
                                                                      'onchange': 'hide()'})})
