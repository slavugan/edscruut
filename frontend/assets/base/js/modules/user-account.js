$(document).ready(function () {
    $('#del-subscr').on('click', function () {
        var subscriptions = $('input[name="subscr"]:checked');
        var ids = [];
        $('input[name="subscr"]:checked').each(function () {
            ids.push($(this).data('id'));
        });

        if (ids.length) {
            $.ajax({
                method: 'post',
                url: '/users/delete-subscriptions/',
                dataType: 'json',
                data: {subscriptions: JSON.stringify(ids)},
            }).done(function () {
                subscriptions.closest('label').remove();
                if ($('input[name="subscr"]').length == 0) {
                    $('#subscr-list').hide();
                    $('#no-subscr').show();
                }
            }).fail(function (resp) {
                elert('Error!')
                console.log(resp);
            });
        }
    });
});