$(document).ready(function() {
    $('#add-to-notification').on('click', function() {
        if ($('#sign-in').length) {
            $('#login-form').modal('show');
        } else {
            $.post(
                djangoContext.urls.subscribe,
                data={filters: JSON.stringify(djangoContext.realtyDetailSubscriptionData)}
            ).done(function() {
                $('#subscribed-mess').modal('show');
            }).fail(function() {
                console.log('subscription failed');
            });
        }
    });

    $('#subscribe-btn').on('click', function() {
        if ($('#sign-out').length) {
            $('#subscribe-dialog').modal('show');
        } else {
            $('#login-form').modal('show');
        }
    });
});

