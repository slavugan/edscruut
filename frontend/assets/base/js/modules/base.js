$(document).ready(function () {
    $('.change-lang').on('click', function () {
        var lang = this.getAttribute('data-lang-code');
        var form = $('#lang-form');
        form.find('input[name="language"]').val(lang);
        form.submit();
    });

    $('#subscribe-btn').on('click', function () {
        if ($('#sign-out').length) {
            $('#subscribe-dialog').modal('show');
        } else {
            $('#login-form').modal('show');
        }
    });

    $('#subscribe-dialog button.cancel').on('click', function (e) {
        if (window.location.pathname.indexOf('search-list') == -1) {
            window.location.href = '/search-list/';
        }
    });

    $("#form-login-modal, #form-signup-modal").submit(function (e) {
        var input = $('<input />').attr('type', 'hidden')
            .attr('name', "next")
            .attr('value', location.pathname + location.search);
        input.appendTo(this);
    });
    $(".socialaccount_provider").attr('next', get_next_path());
    $(".socialaccount_provider").attr('href', $(".socialaccount_provider").attr('href') + "&next=" + get_next_path());
    $(".socialaccount_provider").click(function () {
        $(".socialaccount_provider").attr('href', "/accounts/facebook/login/?process=login&next=" + get_next_path());
        $(".socialaccount_provider").attr('next', "/accounts/facebook/login/?process=login&next=" + get_next_path());
    });
});

function get_next_path() {
    return '/users/social_login_redirect/?url=' + location.pathname + location.search.replace('?', '|').replace(/&/g, '^');
}

function setCookie(key, value, days) {
    var expires = new Date();
    days = days || 60;
    expires.setTime(expires.getTime() + (days * 24 * 60 * 60 * 1000));
    document.cookie = key + '=' + value + '; path=/' + '; expires=' + expires.toUTCString();
}

function csrfSafeMethod(method) {
    // these HTTP methods do not require CSRF protection
    return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
}

$.ajaxSetup({
    crossDomain: false,
    beforeSend: function (xhr, settings) {
        if (!csrfSafeMethod(settings.type)) {
            xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
        }
    }

}); 